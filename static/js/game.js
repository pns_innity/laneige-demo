$(document).ready(function () {
    //initForm();
    //initGame();
    $('.btn-box-close').click(backtoInitial);
    $('#btn-howtoplay').click(gotoHowToPlay1);
    $('#btn-next-step1').click(gotoHowToPlay2);
    $('#btn-next-step2').click(gotoHowToPlay3);
    $('#btn-next').click(gotoIntro);
    //
    //$('#btn-start-game').click(gotoGame);
    $('#btn-play').click(gotoIntro);
    $('#btn-join').click(gotoForm);
    $('.btn-replay').click(replayGame);
    $('#btn-submit').click(confirmSubmitScore);
    $('#btn-confirm').click(submitScore);

    init();
    initForm();

    $('#contact').keydown(function (e) {
        var key = e.charCode || e.keyCode || 0;
        $text = $(this); 
        if (key !== 8) {
            if ($text.val().length === 4) {
                $text.val($text.val() + '-');
            }
        }
      
 
        return (key == 8 || key == 9 || key == 46 || (key >= 48 && key <= 57) || (key >= 96 && key <= 105));
    })

    function backtoInitial(e) {
        setTimeout(function () {      
            $(".two-stars-box").addClass('d-none');         
        }, 1000);
        $('.initial-box').removeClass('d-none');
        gsap.to(".two-stars-box", {
            opacity: 0,
            "left": "-100%",
            duration: 0.5
        });
        gsap.to(".initial-box", {
            opacity: 1,
            "left": "3%",
            duration: 0.5,
            delay: 0.5,
        });
        gsap.to(".two-stars-box", {
            opacity: 0,
            "left": "200%",
            duration: 0.5
        });
    }
    function gotoHowToPlay1() {
        setTimeout(function () {      
            $(".initial-box").addClass('d-none');         
        }, 1000);
        $('.step1-box').removeClass('d-none');
        gsap.to(".initial-box", {
            opacity: 0,
            "left": "-100%",
            duration: 0.5
        });
        gsap.to(".howtoplay-box.step1-box", {
            opacity: 1,
            "left": "50%",
            duration: 0.5,
            delay: 0.5,
        });
        gsap.to(".initial-box", {
            opacity: 0,
            "left": "-100%",
            duration: 0.5,
            delay: 1
        });
    }
    function gotoHowToPlay2() {
        setTimeout(function () {      
            $(".howtoplay-box.step1-box").addClass('d-none');         
        }, 1000);
        $('.step2-box').removeClass('d-none');
        gsap.to(".howtoplay-box.step1-box", {
            opacity: 0,
            "left": "-100%",
            duration: 0.5
        });
        gsap.to(".howtoplay-box.step2-box", {
            opacity: 1,
            "left": "50%",
            duration: 0.5,
            delay: 0.5,
        });
        gsap.to(".howtoplay-box.step1-box", {
            opacity: 0,
            "left": "-100%",
            duration: 0.5,
            delay: 1
        });
    }
    function gotoHowToPlay3() {
        setTimeout(function () {      
            $(".howtoplay-box.step2-box").addClass('d-none');         
        }, 1000);
        $('.step3-box').removeClass('d-none');
        gsap.to(".howtoplay-box.step2-box", {
            opacity: 0,
            "left": "-100%",
            duration: 0.5
        });
        gsap.to(".howtoplay-box.step3-box", {
            opacity: 1,
            "left": "50%",
            duration: 0.5,
            delay: 0.5,
        });
        gsap.to(".howtoplay-box.step2-box", {
            opacity: 0,
            "left": "-100%",
            duration: 0.5,
            delay: 1
        });
    }
    function gotoIntro() {
        setTimeout(function () {      
            $(".initial-box "," .step3-box").addClass('d-none');         
        }, 1000);
        $('.introduction-box').removeClass('d-none'),1000;
        gsap.to(".initial-box , .step3-box", {
            opacity: 0,
            "left": "-100%",
            duration: 0.5
        });
        gsap.to(".introduction-box", {
            opacity: 1,
            "left": "50%",
            duration: 0.5,
            delay: 0.5,
        });
        gsap.to(".initial-box , .step3-box", {
            opacity: 0,
            "left": "-100%",
            duration: 0.5,
            delay: 1
        });
    }
    function gotoForm() {
        setTimeout(function () {      
            $(".introduction-box").addClass('d-none');         
        }, 1000);
        $('.form-box').removeClass('d-none');
        gsap.to(".introduction-box", {
            opacity: 0,
            "left": "-100%",
            duration: 0.5
        });
        gsap.to(".form-box", {
            opacity: 1,
            "left": "50%",
            duration: 0.5,
            delay: 0.5,
        });
    }
    function gotoGame(e) {
        var tl = gsap.timeline();
        var height = $(window).height();
        tl.to(".form-box", {
            opacity: 0,
            "left": "-100%",
            duration: 0.5,
        })
        tl.to(".intro-section", {
            "background-size": "100%",
            "background-position": "50% 0%",
            duration: 1,
        })
        .to(".dreaming .dream-dot1", {
            opacity: 1,
            duration: 0.5,
        })
        .to(".dreaming .dream-dot2", {
            opacity: 1,
            duration: 0.5,
        })
        .to(".dreaming .dream-dot3", {
            opacity: 1,
            duration: 0.5,
        })
        .to(".dreaming .dream-on-top", {
            opacity: 1,
            "top": "0",
            duration: 1,
        })
        .to(".game-section", {
            "height": height,
            duration: 4,
        })
        .to(".intro-section , .dreaming", {
            display: "none",
            onComplete: startGame,
        })
     
    }
    function startGame(e) {
        var tl = gsap.timeline();
        tl.to(".count-3", {
            opacity: 1,
            duration: 0.5,
        })
        .to(".count-3", {
            opacity: 0,
            duration: 0.5,
        })
        .to(".count-2", {
            opacity: 1,
            duration: 0.5,
        })
        .to(".count-2", {
            opacity: 0,
            duration: 0.5,
        })
        .to(".count-1", {
            opacity: 1,
            duration: 0.5,
        })
        .to(".count-1", {
            opacity: 0,
            duration: 0.5,
            onComplete:game.start
        })
        
        //game.start();
    }
    function endGame() {
        var tl = gsap.timeline();
        $('.times-up-box').removeClass('d-none');
        tl.to(".overlay", {
            opacity: 1,
            duration: 0.5,
        })
        tl.to(".times-up-box", {
            duration: 1,
            "top": "0%",
            y: "0%",
            ease: "elastic",
        })
    }
    function confirmSubmitScore() {
        var tl = gsap.timeline();
        setTimeout(function () {      
            $(".times-up-box").addClass('d-none');         
        }, 1000);

        $('.confirm-box').removeClass('d-none');
        tl.to(".times-up-box", {
            duration: 0.5,
            "top": "-200%",
            y: "5%",
            ease: "power3",
        })
        .to(".confirm-box", {
            duration: 1,
            "top": "0%",
            y: "5%",
            ease: "power3",
        })
    }
    function submitScore() {
        var tl = gsap.timeline();
        setTimeout(function () {      
            $(".confirm-box").addClass('d-none');         
        }, 1000);

        $('.thankyou-box').removeClass('d-none');
        tl.to(".confirm-box", {
            duration: 0.5,
            "top": "-200%",
            y: "5%",
            ease: "power3",
        })
        .to(".thankyou-box", {
            duration: 1,
            "top": "0%",
            y: "5%",
            ease: "power3",
        })

        finalSubmitAjax();
    }
    function replayGame(e) {
        var tl = gsap.timeline();
        setTimeout(function () {      
            $(".confirm-box" , ".times-up-box").addClass('d-none');         
        }, 1000);
        tl.to(".confirm-box, .times-up-box", {
            duration: 0.5,
            "top": "-200%",
            "transform":"translate(-50%, 5%)",
            y: "5%",
            ease: "power3",
            
        })
        .to(".overlay", {
            opacity: 0,
            duration: 0.5,
            onComplete:startGame,
        })
        
    }

    var form;

    function initForm() {
        form = new JqForm({
            url: 'https://contenthub.innity-asia.com/2021/sg/laneige/staging/custom/game_register_action.php',
            targetId: '#form1',
            autoSubmit: true,
            errorTextId: null,
            callback: {
                success: gotoGame,
                // fail: error(data),
                // exists: error(data)
            }
        });
        
    }
    
    
    var game;
	
	function init() {
		game = new DropAndCatch({
            fps: 30,
            sensor: {
                id: '#scene'
            },
            scene: {
                id: '#scene'
            },
            catcher: {
                id: '#character',
                speed: 15,
                moveVertical: false,
                hasDepth: true, //for bucket, box, bowl, etc
                depthOffY: 0,
                hit: '.walter',
            },
            item: {
                containerId: '#items',
                class: '.items',
                hit: '.item',
                total: 9,
                speed: [
                    { min: 6, max: 10 },
                    { min: 9, max: 13 }
                ],
                minY: -1000,
                data: [
                    {cssClass:'i1',point:10},
                    {cssClass:'i1',point:10},
                    {cssClass:'i2',point:-5},
                    {cssClass:'i3',point:-5},
                    {cssClass:'i4',point:-5},
                    {cssClass:'i5',point:-5},
				]
            },
            effect: null,
            time: {
                max: 30,
                id: '#score_time'
            },
            score: {
                id: '#score_point',
            },
            controls: {
                enableMouse: true,
                left: 37,
                right: 39
            },
            callback: {
                onCursorMove: null,
                onUpdateFrame: null,
                onScoreChanged: null,
                onFinish: onFinishGame
            }
        });
	}
    
    var final_score=0;

	function onFinishGame(data) {

        let emailData=document.getElementById("email").value;

        let params={
            'email':emailData,
            'score':data.score
        }
        final_score=data.score;

        $.ajax({
            type: 'post',
            url: 'https://contenthub.innity-asia.com/2021/sg/laneige/staging/custom/game_action.php',
            data: params,
            dataType: 'json',
            complete: function ($data) {
            }
        });
        
        endGame();
        $('#final_score').html(data.score);
        $('.header').css({'z-index':'1'});
    }

    function finalSubmitAjax(){
        let emailData=document.getElementById("email").value;

        let params={
            'email':emailData,
            'score':final_score
        }

        $.ajax({
            type: 'post',
            url: 'https://contenthub.innity-asia.com/2021/sg/laneige/staging/custom/game_submit_score_action.php',
            data: params,
            dataType: 'json',
            complete: function ($data) {

            }
        });
    }

});



// -------------------------------- BOX ROLL OVER EFFECT --------------------------------------- //

$(function(){
	boxRollovers();
});

function boxRollovers()
{
	$selector = $(".initial-box");
	XAngle = 0;
	YAngle = 0;
	Z = 0;
	
	$selector.on("mousemove",function(e){
		var $this = $(this);
		var XRel = e.pageX - $this.offset().left;
		var YRel = e.pageY - $this.offset().top;
		var width = $this.width();
		
		YAngle = -(0.2 - (XRel / width)) * 10; 
		XAngle = (0.2 - (YRel / width)) * 10;
		updateView($this.children(".dream-box"));
	});
	
	$selector.on("mouseleave",function(){
		oLayer = $(this).children(".dream-box");
		oLayer.css({"transform":"perspective(525px) translateZ(0) rotateX(0deg) rotateY(0deg)","transition":"all 150ms linear 0s","-webkit-transition":"all 150ms linear 0s"});
		oLayer.find("strong").css({"transform":"perspective(525px) translateZ(0) rotateX(0deg) rotateY(0deg)","transition":"all 150ms linear 0s","-webkit-transition":"all 150ms linear 0s"});
	});		
}

function updateView(oLayer){
	oLayer.css({"transform":"perspective(525px) translateZ(" + Z + "px) rotateX(" + XAngle + "deg) rotateY(" + YAngle + "deg)","transition":"none","-webkit-transition":"none"});
	oLayer.find("strong").css({"transform":"perspective(525px) translateZ(" + Z + "px) rotateX(" + (XAngle / 0.66) + "deg) rotateY(" + (YAngle / 0.66) + "deg)","transition":"none","-webkit-transition":"none"});
}


