jQuery(document).on('submit', '.sh-comment-input form', function () {
    var obj = $(this);
    var comment = $(obj).find('textarea[name="comment"]').val();
    var nid = $(obj).find('input[name="nid"]').val();

    var node_obj = $(this).parents(shPrimaryArea);
    var nid = node_obj.attr('id').replace('node-', '');
    var post_image = node_obj.data('image');
    var post_title = node_obj.data('title');
    var post_link = node_obj.data('link');
    var post_source = node_obj.data('source');
	var post_content_type = node_obj.data('content-type');

    if (comment.trim() == '') {
        jQuery(".form-comment-form textarea[name='comment']").focus();
        return false;
    }

    ga('send', 'event', 'social post', 'comment enter', 'node/' + nid + agent);
    ga('clientTracker.send', 'event', 'social post', 'comment enter', 'node/' + nid + agent);

    if (typeof(filterWords)!='undefined') {
        comment = wordFilter(comment);
    }

    $(obj).find('textarea[name="comment"]').val('');

    jQuery.ajax({
        type: "POST",
        dataType: 'json',
        url: base_url + 'api.php?endpoint=' + encodeURIComponent('comment'),
        data: {uid: user.uid, nid: nid, comment: comment},
        xhrFields: {
            withCredentials: true
        },
        beforeSend: function () {
            var text = '<div class="row-comment row-comment-output"><div class="col-profile-image"><div class="profile-image" style="background-image: url(' + user.photoURL + ')"></div></div><div class="col-message"><div class="name">' + user.name + '</div><div class="message message-output">' + comment + '</div></div></div>';
            $(text).insertBefore('.sh-status-yes');
            $(obj).parents(shPrimaryArea).find('.sh-comment-box-holder').scrollTop($('.sh-comment-box').height());

            if($('.sh-comment-nocomment').length){
                $('.sh-comment-nocomment').hide();
            }
        },
        success: function () {
            $.get(base_url + 'cache.php?wildcard=' + encodeURIComponent('comment&content_id=' + nid + '&brand_id=' + brand_id + '&user_type=uph'));
            if (typeof(Gamification) != 'undefined' && typeof(GAME_CODE) != 'undefined' ) {
                Gamification.gamificationAction(GAME_CODE.COMMENT, {link: 'post.php?nid=' + nid});
            }
        }
    });

    return false;
});
