var callback_obj;

var callback = {
   'object': {},
   'action': ''
};

var user = {uid: 0};

var node = {};
var reloadFirst = false;

var agent = '_desktop';

let searchParams = new URLSearchParams(window.location.search);

var isMobileDevice = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobileDevice.Android() || isMobileDevice.BlackBerry() || isMobileDevice.iOS() || isMobileDevice.Opera() || isMobileDevice.Windows());
    }
};

if(isMobileDevice.Android() || isMobileDevice.BlackBerry() || isMobileDevice.iOS() || isMobileDevice.Opera() || isMobileDevice.Windows()){
    agent = '_mobile';
}

systemConnect();

var temporaryURL;

// sh admin default value
if (typeof (moderateComment) === 'undefined') {
    var moderateComment = true;
}
if (typeof (manageContent) === 'undefined') {
    var manageContent = true;
}
if (typeof (manageBanner) === 'undefined') {
    var manageBanner = true;
}
if (typeof (reports) === 'undefined') {
    var reports = true;
}
if (typeof (convertToAd) === 'undefined') {
    var convertToAd = true;
}
// end of sh admin default value

var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

//    after the API code downloads.
var player;
var isReady = 0;
var ytVideoDone = false;
var ytVideoPause = false;
var ytSecWatch = [];
var ytInterval;
var ytDuration = 0;
var ytMinWait = 60;

var shPrimaryArea = '.sh-main';

function onYouTubeIframeAPIReady() {
    isReady = 1;
}
// 4. The API will call this function when the video player is ready.
function onPlayerReady(event) {
   ytVideoPause = false;
   ytVideoDone = false;


    ytDuration = Math.ceil(ytplayer.getDuration());
    if(ytDuration<ytMinWait) ytMinWait = ytDuration;
    //console.log(ytDuration, ytMinWait);

    $('#btn_ytmute').click(function(e){
        if(ytplayer.isMuted()) { ytplayer.unMute(); $(e.target).toggleClass('muted'); }
        else { ytplayer.mute(); $(e.target).toggleClass('muted'); }
    });
}

function onPlayerStateChange(event) {
    if (event.data == YT.PlayerState.ENDED) {
        ytVideoDone = true;
        
        clearInterval(ytInterval);
    }

    if (event.data == YT.PlayerState.PAUSED && !ytVideoDone) {
        ytVideoPause = true;
        
        clearInterval(ytInterval);
    }

    if (event.data == YT.PlayerState.PLAYING && !ytVideoPause) {
        ytVideoPause = false;

        clearInterval(ytInterval);
        ytSecWatch = [];
        ytInterval = setInterval(onPlayerPlaying,1000);
    }
}

function onPlayerPlaying() {

    var curTime = Math.round(ytplayer.getCurrentTime());
    ytSecWatch.push(curTime);

    if(ytSecWatch.length>=ytMinWait) {
        clearInterval(ytInterval);
        //alert('get video points!');
    }
}

function stopVideo() {
    if (typeof (player) != 'undefined') {
        player.stopVideo();
    }
}

/*-- GET PARAMETER --*/
function getURLParameter(name) {
    return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null;
}

function clearSession() {
    var name = "SESS";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) {
            document.cookie = c.substring(0, c.indexOf("="))+"=;domain=.socialhub.innity-asia.com;path=/; expires=Thu, 01 Jan 1970 00:00:00 UTC";
            console.log(c.substring(0, c.indexOf("="))+"=; expires=Thu, 01 Jan 1970 00:00:00 UTC");
        }
    }
}

function systemConnect() {
    if (!getCookie('reload_first')) {
        reloadFirst = true;
    }
    checkLogin();
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return false;
}

function likePost(nid) {
    var obj = $('#node-' + nid + ', #modalSocialpost #node-' + nid).find('.like');
    var node_obj = $('#node-' + nid);
    var nid = node_obj.attr('id').replace('node-', '');
    var post_image = node_obj.data('image');
    var post_title = node_obj.data('title');
    var post_link = node_obj.data('link');
    var post_source = node_obj.data('source');
    var post_content_type = node_obj.data('content-type');


    $.ajax({
        type: 'POST',
        url: 'http://localhost/laneige-demo/api/api.php?endpoint=' + encodeURIComponent('like'),
        data: {uid: user.uid, nid: nid},
        beforeSend: function () {
            $(obj).hasClass('active') ? $(obj).removeClass('active') : $(obj).addClass('active');
        },
        xhrFields: {
            withCredentials: true
        },
        success: function (responseData, textStatus, jqXHR) {
            if (responseData != 0) {
                if (typeof(Gamification) != 'undefined' && typeof(GAME_CODE) != 'undefined' && post_content_type != 'uphplus_contributor_canal_post') {
                    Gamification.gamificationAction(GAME_CODE.LIKE, {
                        link : 'http://localhost/laneige-demo/dist/post?nid=' + nid
                    });
                }

                user.liked[nid] = nid;
                $(obj).addClass('active');
                var count = parseInt($('#node-' + nid).find('.like_count').text()) + 1;
                $('#node-' + nid).find('.like_count').html(count);
            }
        }
    });
}

function postIsLiked(nid) {
    if (typeof (user.liked) != 'undefined' && typeof (user.liked[nid]) != 'undefined') {
        return true;
    }
    return false;
}

function postLikedGet() {
    if (!user.uid) {
        return false;
    }
    $.ajax({
        type: 'GET',
        url: 'http://localhost/laneige-demo/api/api.php?endpoint=' + encodeURIComponent('like'),
        data: {uid: user.uid},
        dataType: 'json',
        xhrFields: {
            withCredentials: true
        },
        success: function (responseData, textStatus, jqXHR) {
            user.liked = {};
            if (responseData.data.length) {
                $.each(responseData.data, function (i, v) {
                    var content_id = v.content_id.toString();
                    user.liked[content_id] = content_id;
                });

                if ($('.box').length) {
                    $.each($('.box'), function () {
                        if (typeof ($(this).attr('id')) != 'undefined') {
                            if (postIsLiked(getNodeId($(this).attr('id')))) {
                                $(this).find('.like').addClass('active');
                            }
                        }
                    });
                }
            }
        }
    });
}

function postIsCommented(nid) {
    if (typeof (user.commented) != 'undefined' && typeof (user.commented[nid]) != 'undefined') {
        return true;
    }
    return false;
}

function postCommentedGet() {
    if (!user.uid) {
        return false;
    }
    $.ajax({
        type: 'GET',
        url: 'http://localhost/laneige-demo/api/api.php?endpoint=' + encodeURIComponent('comment'),
        data: {uid: user.uid},
        dataType: 'json',
        xhrFields: {
            withCredentials: true
        },
        success: function (responseData, textStatus, jqXHR) {
            user.commented = {};
            if (responseData.data.length) {
                $.each(responseData.data, function (i, v) {
                    var content_id = v.content_id.toString();
                    user.commented[content_id] = content_id;
                });

                if ($('.box').length) {
                    $.each($('.box'), function () {
                        if (typeof ($(this).attr('id')) != 'undefined') {
                            if (postIsCommented(getNodeId($(this).attr('id')))) {
                                $(this).find('.comment').addClass('active');
                            }
                        }
                    });
                }

                if ($('.featured-box').length) {
                    if (typeof ($('.featured-box').attr('id')) != 'undefined') {
                        if (postIsCommented(getNodeId($('.featured-box').attr('id')))) {
                            $('.featured-box').find('.comment').addClass('active');
                        }
                    }
                }

            }
        }
    });
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function engageplus_login_callback(obj) {
    if (typeof (obj) != 'undefined' && typeof (obj.uid) != 'undefined') {
        user = obj;
    }

    afterLogin();
    checkLogin();
}

function afterLogin(){
    $('.sh-comment-image').css('background-image', 'url(' + user.photoURL + ')');
}

function checkLogin() {

    if (typeof (user) == 'undefined' || user.uid == 0) {

        $('.nav-logout').hide();
    } else {
        $('.nav-login').hide();
        $('.nav-logout').show();
        $('#profileImage').css('background-image', 'url(' + user.photoURL + ')');
        $('#profileImageMobile').css('background-image', 'url(' + user.photoURL + ')');
        $('.profile-name').html(user.name);

        postLikedGet();
    }

}


function userLogout() {
    document.cookie = "reload_first=1; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/";
    Gamification.logout();
    window.location.href = base_url;
}


function defaultAuthor(obj) {
    $(obj).attr("src", user_default_image);
}

function defaultAuthorImage() {
    $('.box .author .image img').error(function () {
        $(this).attr('src', user_default_image);
    });
}

function dotdot() {
    $(".box .sh-txt-desc").dotdotdot({
        watch: 'window',
        ellipsis: ' ...',
        wrap: 'word'
    });

    $("#boxes .box .shared-link .link span").dotdotdot({
        watch: 'window',
        ellipsis: ' ...',
        wrap: 'letter'
    });
}

function dateFriendly() {
    $('.date').each(function () {
        if ($(this).attr("timestamp")) {
            $(this).append(timeAgoFromEpochTime($(this).attr("timestamp") / 1000));
            $(this).addClass('processed');
        }
    });
}


function fn(name, arguments) {
    var fn = window[name];
    if (typeof fn !== 'function') {
        return;
    }
    fn.apply(window, arguments);
}

function getNodeId(str){
    return str.replace('node-', '');
}

function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

function updateQueryStringParameter(uri, key, value) {
    var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
    var separator = uri.indexOf('?') !== -1 ? "&" : "?";
    if (uri.match(re)) {
        return uri.replace(re, '$1' + key + "=" + value + '$2');
    } else {
        return uri + separator + key + "=" + value;
    }
}

function urlShorten(url) {
    var shorten = '';
    jQuery.ajax({
        url: base_url + 'api.php?api=' + encodeURIComponent(api_path + '/tools/url_shortener.json') + '&token=' + encodeURIComponent('1'),
        data: {'url': url},
        dataType :'json',
        method: 'post',
        async: false,
        success: function (data) {
            shorten = data.data.id;
        }
    });
    return shorten;
}

function smartTrim(str, length, delim, appendix) {
    if (str.length <= length) {
        return str;
    }

    var trimmedStr = str.substr(0, length + delim.length);

    var lastDelimIndex = trimmedStr.lastIndexOf(delim);
    if (lastDelimIndex >= 0) {
        trimmedStr = trimmedStr.substr(0, lastDelimIndex);
    }

    if (trimmedStr) {
        trimmedStr += appendix;
    }
    return trimmedStr;
}

function getURL(url) {
    url = url + '&ajax=1';

    $.ajax({
        type: 'GET',
        url: url,
        cache: false,
        data: {},
        success: function (data, textStatus, jqXHR) {

            $("#modalSocialpost .modal-content").empty();
            $('#modalSocialpost .modal-content').html($(data));
            $("#modalSocialpost").modal('show');

            if (!temporaryURL) {
                temporaryURL = location.href;
            }

            var seotitle = $('#modalSocialpost .modal-content .node').attr('seotitle');
            var nid = $('#modalSocialpost .modal-content .node').attr('id');
            nid = nid.replace('node-', '');
            param = !window.location.href.indexOf("/post/") >= 0 ? param = base_url + 'post/' + nid + '/' + seotitle: '' ;

            setTimeout(function(){
                if (user.uid) {
                    jQuery(".commentarea input" ).focus();
                }
                resizeVideoDiv();
                window.history.replaceState('nid', 'nodeID', param);
				if (SH_fb_share) {
					if (typeof(Gamification) != 'undefined' && typeof(GAME_CODE) != 'undefined') {
						Gamification.gamificationAction(GAME_CODE.SHARE, {
							link : 'post.php?nid=' + nid
						});
					}
				}
            }, 1700);


            if (typeof (user) == 'undefined' || user.uid == 0) {
                $('.sh-status-yes').hide();
            } else {
                $('.sh-status-none').hide();
                $('.sh-status-yes').show();
                $('.sh-comment-image').css('background-image', 'url(' + user.photoURL + ')');
            }


        }
    });
}


function resizeVideoDiv() {
    if($('.video-facebook').length){
        $('.video-facebook-iframe iframe').height($('.video-facebook-trans img').height() + 1);
        var fbVideoInterval = setInterval(function(){
            if ($("#iframeFacebook").contents().find(".fb-video").find("iframe").css("visibility") == 'visible') {
                var videoBottomHeight = parseInt($("#iframeFacebook").contents().find(".fb-video").find("iframe").css("height"));
                $('#iframeFacebook').css('height', videoBottomHeight);
                $('.modal#modal-fullscreen .sh-main-full .video-facebook-iframe').css('height', videoBottomHeight);
                clearInterval(fbVideoInterval);
            }
        }, 1000);
    }
}

function videoResize(){

  if($('.video-instagram').length){
    $('.video-instagram').height($('.EmbeddedMediaImage').height() + 1);
    var instaVideoInterval = setInterval(function(){
        if ($(".Embed").contents().find(".EmbeddedMediaImage").css("visibility") == 'visible') {
            var videoBottomHeight = parseInt($(".EmbeddedMedia img").contents().find(".EmbeddedMediaImage").height());
            $('#iFrameInstagram').css('height', videoBottomHeight);
            $('.modal#post_boxes .sh-main-full .video-instagram').css('height', videoBottomHeight);
            clearInterval(instaVideoInterval);
        }

    }, 1000);
  }

}

/*** DOCUMENT READY ***/

$(document).ready(function(){

    $('#loadmore').hide();

    $('#modalSocialpost').on('hidden.bs.modal', function () {
        if ($('.modal#modalSocialpost').length) {
            $('.modal#modalSocialpost .modal-content').empty();
            $('.modal#modalSocialpost').modal('hide');
        }
        param = temporaryURL;
        window.history.replaceState('nid', 'removenodeID', param);
    });
// $('#form_manual_register .reg_password_wrapper').hide();
    $('#modalEvent').on('hidden.bs.modal', function () {
        if ($('.modal#modalEvent').length) {
            $('.modal#modalEvent .modal-content').empty();
            $('.modal#modalEvent').modal('hide');
        }
        param = temporaryURL;
        window.history.replaceState('nid', 'removenodeID', param);
    });

    $('.modalShareSm').on('hidden.bs.modal', function () {
        if ($('.modal.modalShareSm').length) {
            $('.modal.modalShareSm .modal-content').empty();
            $('.modal.modalShareSm').modal('hide');
        }
        param = temporaryURL;
        window.history.replaceState('nid', 'removenodeID', param);
    });

    /*if (SH_postNid) {
        getURL('node.php?nid='+SH_postNid);
    }*/

	
	/*if (SH_fb_share_site) {
        if (typeof(Gamification) != 'undefined' && typeof(GAME_CODE) != 'undefined' ) {
			Gamification.gamificationAction(GAME_CODE.SHARE_SITE);
			setTimeout(function() {
				var replacePath = location.protocol + '//' + location.host + location.pathname;
				window.history.replaceState( {} , 'path', replacePath );
			}, 500);
		}
    } */

    $(window).resize(function() {
        //$('.menu-bear').hide();
        if(this.resizeTO) clearTimeout(this.resizeTO);
        this.resizeTO = setTimeout(function() {
            $(this).trigger('resizeEnd');
        }, 500);
    });

    $(window).bind('resizeEnd', function() {
        /** Resize video div height **/
        resizeVideoDiv();
        // if ($('.popup-menu').hasClass('visible')) {
        //     if ($(window).height() < 500) {
        //         $('.menu-bear').hide();
        //     }
            // else {
            //     $('.menu-bear').delay(300).fadeIn(1000);
            // }
        // }
        // else {
        //     $('.menu-bear').hide();
        // }

        /** End Resize video div height **/
    });

    // Set up infinitescroll
    var token = $('#after_token').data('token');
    var next_page_url = $('#page-nav a').attr('href');
    var $container = $('#boxes');
    var currPage = 1;

    $container.infiniteScroll({
        append: '#boxes .shbox',
        path: function() {return next_page_url;},
        button: "#loadmore",
        // path: '#page-nav a',
        // elementScroll: '.row-video-wrapper',
        history: false
    });

    var infScroll = $container.data('infiniteScroll');
    // Triggered when the next page has been successfully loaded, but not yet added to the container.
    $container.on( 'load.infiniteScroll', function(event, response, path) {
        if (currPage >=1 && token != '') {
            $('#loadmore').show();
        }

        console.log(currPage);
        console.log(token);

        // if (token != "") {
            $('#loadmore').addClass('active');
        // }
        token = $("#after_token", $(response)).data("token");
        next_page_url = $(response).find('#page-nav a').attr('href');

        currPage = infScroll.pageIndex;

        $container.infiniteScroll( 'option', {
            loadOnScroll: false,
        });
        $('#loadmore').removeClass('active');

        // if (currPage == 3) {
        //     $container.infiniteScroll( 'option', {
        //         loadOnScroll: false,
        //     });
        //     $('#loadmore').removeClass('active');
        // }

        if (token == "") {
             $container.infiniteScroll( 'option', {
                loadOnScroll: false,
            });
            $('#loadmore').addClass('no_more_post').hide();
        }

        var $posts = $(response).find('.shbox');
        $container.infiniteScroll('appendItems', $posts);

    });

    $container.on('append.infiniteScroll', function( event, response, path, items ) {
        ga('send', 'event', 'scroll page', 'brand/' + brand_id, 'page ' + currPage);
        ga('clientTracker.send', 'event', 'scroll page', 'brand/' + brand_id, 'page ' + currPage);

        $('#boxes .shbox').each(function(){
            if (postIsLiked(getNodeId($(this).attr('id')))) {
                $(this).find('.like').addClass('active');
            }

            if (postIsCommented(getNodeId($(this).attr('id')))) {
                $(this).find('.comment').addClass('active');
            }
        });

    });

    $('#loadmore').click(function(){
        $(this).addClass('active');
        $container.infiniteScroll( 'option', {
            loadOnScroll: true,
        });
        $container.infiniteScroll('loadNextPage');
    });

    jQuery(document).on('click', '.tt-read-more', function(){
        //  by using the return-value...
        var content = $(".tt-desc-wrapper").triggerHandler("originalContent");
        // $('.tt-read-more').hide();
        console.log($(content));

        // $(".tt-desc-wrapper").empty().append( content );


    });

    /*** MAIN FUNCTION (OPEN & CLOSE) ***/
    jQuery(document).on('click', '.sh-open', function(){
        var url = $(this).attr('href');
        var pathArrayURL = url.split( '/' );
        var type = $(this).attr('type');

        if (type == 'facebook' || type == 'instagram' || type == 'youtube'|| type == 'uphplus_video') {
            getURL(base_url+'node.php?nid='+pathArrayURL[pathArrayURL.length - 2]);
        }


        if (typeof(big_banner_slider) != 'undefined' && bigBannerTotal > 1) {
            big_banner_slider.stopAuto();
        }

        return false;
    });

    jQuery(document).on('click', '.sh-post-open', function(){
        var url = $(this).attr('href');
        var pathArrayURL = url.split( '/' );

        window.location = base_url + pathArrayURL;
        return false;
    });

    jQuery(document).on('click', '.sh-open-external', function(){
        var url = $(this).attr('url');

        if (typeof(url) != 'undefined') {
            window.open(url);
        }

        return false;
    });

    jQuery(document).on('click', '.sh-close', function(){
        if ($(this).hasClass('noajax')) {
            window.location.href = base_url;
            return false;
        }

        if ($('#modalSocialpost').length) {
            $('#modalSocialpost .modal-body').empty();
            $('#modalSocialpost').modal('hide');
        }

        param = temporaryURL;
        window.history.replaceState('nid', 'removenodeID', param);
        // temporaryURL = '';

        return false;
    })

    /*** END MAIN FUNCTION (OPEN & CLOSE) ***/

    /*** SHARE ***/
    jQuery(document).on('click', '.sh-share', function() {
        /*if(!user.uid){
            $('.modalLogin2').modal('show');
            return false;
        } else if (!user.registered){
            var type = Object.keys(user.providers)[0];
             if (type == 'manual') {
                $('#form_manual_register .reg_password_wrapper').hide();
             }else{
                //$("#form_manual_register #remail").prop("readonly",true);
                $('#form_manual_register .reg_password_wrapper').hide();
             }
            $('.modalSignup').modal('show');
            return false;
        } else if (!user.activate) {
            $('.modalResetPassword2').modal('show');
            return false;
        }*/
        console.log('click');

        var node_obj = $(this).parents(shPrimaryArea);
        var nid = node_obj.attr('id').replace('node-', '');
        var post_image = node_obj.data('image');
        var post_title = node_obj.data('title');
        var post_link = node_obj.data('link');
        //var post_source = node_obj.data('source');
        var post_content_type = node_obj.data('content-type');

        if ($(this).attr('platform') == 'facebook') {
        /*** SHARE FACEBOOK ***/
        link = post_link+'/trafficsource/fb';
        // link = urlShorten(link);


		var isChrome = navigator.userAgent.includes("Chrome") && navigator.vendor.includes("Google Inc");

		if (isChrome == true && agent == '_mobile') {
			window.open('https://www.facebook.com/dialog/feed?app_id=289414475309249&display=popup&link=' + link + '&redirect_uri=' + link + (post_content_type != 'uphplus_contributor_canal_post' ? '?fb_share=1' : ''));
		} else {
			FB.ui({
				method: 'feed',
				link: link,
				caption: '',
				message: ''
			}, function (response) {
				if (response && !response.error_message) {
					if (typeof(Gamification) != 'undefined' && typeof(GAME_CODE) != 'undefined' && post_content_type != 'uphplus_contributor_canal_post') {
						Gamification.gamificationAction(GAME_CODE.SHARE, {
							link : 'post.php?nid=' + nid
						});
					}
				}
			});
		}

        /*** END SHARE FACEBOOK ***/
        } else if ($(this).attr('platform') == 'twitter') {

            /*** SHARE TWITTER ***/

            link = post_link+'/trafficsource/tw';
            //link = urlShorten(link);

            var count_chars = 0;
            count_chars = 200 - parseInt(link.length) - 10;
            desc = smartTrim(post_title, count_chars, ' ', '...');
            desc = encodeURIComponent(desc);
            link = encodeURIComponent(link);

                if (typeof(Gamification) != 'undefined' && typeof(GAME_CODE) != 'undefined' && post_content_type != 'uphplus_contributor_canal_post') {
                    Gamification.gamificationAction(GAME_CODE.SHARE, {
                        link : 'post.php?nid=' + nid
                    });
                }

            window.open('https://twitter.com/intent/tweet?url=' + link + '&text=' + desc );

            /*** END SHARE TWITTER ***/

        } else if ($(this).attr('platform') == 'linkedin') {

            link = post_link+'/trafficsource/in';
            //link = urlShorten(link);

            var count_chars = 0;
            count_chars = 200 - parseInt(link.length) - 10;
            desc = smartTrim(post_title, count_chars, ' ', '...');
            desc = encodeURIComponent(desc);
            link = encodeURIComponent(link);

                if (typeof(Gamification) != 'undefined' && typeof(GAME_CODE) != 'undefined' && post_content_type != 'uphplus_contributor_canal_post') {
                    Gamification.gamificationAction(GAME_CODE.SHARE, {
                        link : 'post.php?nid=' + nid
                    });
                }

            window.open('https://www.linkedin.com/shareArticle?mini=true&url=' + link + '&title=' + desc );

        } else if ($(this).attr('platform') == 'whatsapp') {
            link = post_link+'/trafficsource/wa';
            //link = urlShorten(link);

            var count_chars = 0;
            count_chars = 200 - parseInt(link.length) - 10;
            desc = smartTrim(post_title, count_chars, ' ', '...');
            desc = encodeURIComponent(desc);
            link = encodeURIComponent(link);

                if (typeof(Gamification) != 'undefined' && typeof(GAME_CODE) != 'undefined' && post_content_type != 'uphplus_contributor_canal_post') {
                    Gamification.gamificationAction(GAME_CODE.SHARE, {
                        link : 'post.php?nid=' + nid
                    });
                }

            window.open('https://api.whatsapp.com/send?text=' + link);

        }

        return false;
    })
    /*** END SHARE ***/

    /*** LOGIN & LOGOUT ***/
    jQuery(document).on('click', '.sh-login', function() {

        EngagePlusLogin[309].setCallbackURL(window.location.href);

        if ($(this).attr('platform') == 'facebook') {
            EngagePlusLogin[309].login_action('Facebook');
            console.log('facebook');
        } else if ($(this).attr('platform') == 'twitter') {
            EngagePlusLogin[engageplus_cid].login_action('Twitter');
        } else if ($(this).attr('platform') == 'google') {
            EngagePlusLogin[309].login_action('Google');
        } else if ($(this).attr('platform') == 'linkedin') {
            EngagePlusLogin[engageplus_cid].login_action('LinkedIn');
        }

        return false;
    });

    jQuery(document).on('click', '.sh-logout', function() {
        userLogout();
        return false;
    });

    jQuery(document).on('click', '.sh-logbox-close', function() {
        $('.logbox .popup-fb-login').hide();
        return false;
    });

    jQuery(document).on('click', '.sh-logbox', function() {
        $('#myModal2').modal('show');
        return false;
    });
    /*** END LOGIN ***/

    /*** COMMENT ***/
    jQuery(document).on('keypress', '.sh-comment-input textarea', function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            if (!user.registered){
                var type = Object.keys(user.providers)[0];
                 if (type == 'manual') {
                    $('#form_manual_register .reg_password_wrapper').hide();
                 }else{
                    $('#form_manual_register .reg_password_wrapper').hide();
                 }
                $('.modalSignup').modal('show');
                return false;
                } else if (!user.activate) {
                    $('.modalResetPassword2').modal('show');
                    return false;
            }

            if (!$.trim($(this).val()).length) {
                $(this).css('border', '2px solid red');
                return false;
            }

            $(this).css('border', '0px');

            $(this).parents('form').submit();
            return false;
        }
    });

    jQuery(document).on('click', '.sh-comment-input textarea', function() {
        if(!user.uid){
            $('.modalLogin2').modal('show');
            return false;
        } else if (!user.registered){
            var type = Object.keys(user.providers)[0];
             if (type == 'manual') {
                $('#form_manual_register .reg_password_wrapper').hide();
             }else{
                //$("#form_manual_register #remail").prop("readonly",true);
                $('#form_manual_register .reg_password_wrapper').hide();
             }
            $('.modalSignup').modal('show');
            return false;
        } else if (!user.activate) {
            $('.modalResetPassword2').modal('show');
            //$(".sh-comment-input textarea").prop("disabled",true);
            return false;
        }
    });


    jQuery(document).on('click', '.sh-comment', function() {

        if(!user.uid){
            $('.modalLogin2').modal('show');
            return false;
        } else if (!user.registered){
            var type = Object.keys(user.providers)[0];
             if (type == 'manual') {
                $('#form_manual_register .reg_password_wrapper').hide();
             }else{
                //$("#form_manual_register #remail").prop("readonly",true);
                $('#form_manual_register .reg_password_wrapper').hide();
             }
            $('.modalSignup').modal('show');
            return false;
        } else if (!user.activate) {
            $('.modalResetPassword2').modal('show');
            return false;
        }

        var url = $(this).attr('link');
        var pathArrayURL = url.split( '/' );
        getURL(base_url+'node.php?nid='+pathArrayURL[pathArrayURL.length - 2]);

        return false;
    });

    jQuery(document).on("click", ".col-enter", function(){
       /* here is your clicked button's ID */

        var obj = $('.sh-comment-input form');
        var comment = $(obj).find('textarea[name="comment"]').val().trim();

        if (comment.length==''||comment.length=='0') {
            $(obj).find('textarea[name="comment"]').css('border', '2px solid red');
            return false;
        }
        $(obj).find('textarea[name="comment"]').css('border', '0px');

        $(obj).find('textarea[name="comment"]').parents('form').submit();
        return false;
    });

    /*** END COMMENT ***/

    /*** LIKE ***/
    jQuery(document).on('click', '.sh-like', function(){

        if(!user.uid){
            $('.modalLogin2').modal('show');
            return false;
        }

        if($(this).hasClass('active')){
            return false;
        }

        likePost(getNodeId($(this).parents(shPrimaryArea).attr('id')));

        return false;
    });
    /*** END LIKE ***/

    /** SOURCE **/

    jQuery(document).on('click', '.sh-source', function() {
        var node_obj = $(this).parents(shPrimaryArea);
        var nid = node_obj.attr('id').replace('node-', '');
        
    });
    /** END SOURCE **/

    /*** SEARCH ***/

    jQuery(document).on('keypress', '.sh-search-input', function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            var query = $(this).val();
            if(query.indexOf("#") >= 0){
                query = query.replace('#', '');
            }
          window.location.href = base_url + 'search/' + query;
          return false;
        }
    });

    jQuery(document).on('click', '.sh-search', function (event) {
        var query = $(this).parent().find('.sh-search-input').val();
        if(query.indexOf("#") >= 0){
            query = query.replace('#', '');
        }
        window.location.href = base_url + 'search/' + query;
        return false;
    });
    /*** END SEARCH ***/

    jQuery(document).on('click', '.read-more', function() {
        var elem = $(".read-more").text();
        if (elem == "read more") {

            $(".tt-desc-wrapper").trigger("destroy");
            $(".tt-desc-wrapper").css('visibility', 'visible');
            $(".tt-read-more").show().text("read less");
            // $(".comment-col .desc").addClass("active");
        } else {
            $(".read-more").text("read more");
            $(".tt-desc-wrapper").dotdotdot({
                ellipsis    : '... ',
                wrap        : 'word',
                after       : '.tt-read-more',
                watch       : false,
                height      : 300,
            });

            // $(".comment-col .desc").removeClass("active");
        }
    });

    jQuery(document).on('click', '.sh-share-site', function() {

        if(!user.uid){
            $('.modalLogin2').modal('show');
            return false;
        } else if (!user.registered){
            var type = Object.keys(user.providers)[0];
             if (type == 'manual') {
                $('#form_manual_register .reg_password_wrapper').hide();
             }else{
                //$("#form_manual_register #remail").prop("readonly",true);
                $('#form_manual_register .reg_password_wrapper').hide();
             }
            $('.modalSignup').modal('show');
            return false;
        } else if (!user.activate) {
            $('.modalResetPassword2').modal('show');
            return false;
        }

        var link = base_url;

        if ($(this).attr('platform') == 'facebook') {
            /*** SHARE FACEBOOK ***/
			var isChrome = navigator.userAgent.includes("Chrome") && navigator.vendor.includes("Google Inc");
			if (isChrome == true && agent == '_mobile') {
				window.open('https://www.facebook.com/dialog/feed?app_id=289414475309249&display=popup&link=' + link + '&redirect_uri=' + window.location.href + '?fb_share_site=1');
			} else {
				FB.ui({
					method: 'feed',
					name: '',
					link: link,
					picture: '',
					caption: '',
					description: '',
					message: ''
				}, function (response) {
					if (response && !response.error_message) {
						if (typeof(Gamification) != 'undefined' && typeof(GAME_CODE) != 'undefined' ) {
							Gamification.gamificationAction(GAME_CODE.SHARE_SITE);
						}
					}
				});
			}

            /*** END SHARE FACEBOOK ***/
        } else if ($(this).attr('platform') == 'twitter') {

            link = encodeURIComponent(link);
            if (typeof(Gamification) != 'undefined' && typeof(GAME_CODE) != 'undefined' ) {
                Gamification.gamificationAction(GAME_CODE.SHARE_SITE);
            }
            window.open('https://twitter.com/intent/tweet?url=' + link );


        } else if ($(this).attr('platform') == 'linkedin') {

            link = encodeURIComponent(link);
            if (typeof(Gamification) != 'undefined' && typeof(GAME_CODE) != 'undefined' ) {
                Gamification.gamificationAction(GAME_CODE.SHARE_SITE);
            }
            window.open('https://www.linkedin.com/shareArticle?mini=true&url=' + link );

        } else if ($(this).attr('platform') == 'whatsapp') {

            link = encodeURIComponent(link);
            if (typeof(Gamification) != 'undefined' && typeof(GAME_CODE) != 'undefined' ) {
                Gamification.gamificationAction(GAME_CODE.SHARE_SITE);
            }
            window.open('https://api.whatsapp.com/send?text=' + link);

        }

        return false;
    });

    jQuery(document).on('click', '.tt-welcome', function (event) {
        if(agent == '_desktop'){
            window.location.href = base_url + 'profile';
        }
    });

    jQuery(document).on('click', '.sh-backtotop', function() {
        $('body,html').animate({scrollTop:10},'slow');
        return false;
    });

    dotdot();
    defaultAuthorImage();
    //dateFriendly();


 jQuery(document).on('click', '.htw1', function() {
        if(!user.uid){

            $('.modalLogin2').modal('show');
        }

    });


  jQuery(document).on('click', '.navbar-burger', function() {

        if(!user.uid){
            $('.bullet-profile').hide();
            $('.bullet-leaderboard').hide();
            return false;
        }

    });


  jQuery(document).on('click', '.btn-signup', function() {

        if(!user.uid){
            $('.modalLogin2').modal('show');
            return false;
        }

    });


});

/*** END DOCUMENT READY ***/