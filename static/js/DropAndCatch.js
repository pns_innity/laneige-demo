/*!
 * VERSION: 1.2.0
 * DATE: 10/09/2019
 **/

function DropAndCatch(opts) {

    this.start = start;
    this.restart = restart;
    this.getData = getData;

    var options = mergeObject({
        fps:30,
        sensor: {
            id: null
        },
        scene: {
            id: '#scene'
        },
        catcher: {
            id: '#character',
            speed: 5,
            moveVertical: true,
            hasDepth: true, //for bucket, box, bowl, etc
            depthOffY: 0,
            hit: '.hit',
        },
        item: {
            containerId: '#items',
            class: '.items',
            hit: '.item',
            total: 10,
            speed: null,
            minY:-1000,
            data: null
        },
        effect: null,
        time: {
            max: 30,
            id: null
        },
        score: {
            id: null,
        },
        controls: {
            enableMouse: true,
            left: null,
            right: null
        },
        callback: {
            onCursorMove: null,
            onUpdateFrame: null,
            onScoreChanged: null,
            onFinish: null
        }
    }, opts, 'options');

    if(typeof $ === 'undefined') {
        alert('Please include jquery script');
    }

    var started = false;
    var cursorActive = false;

    var catcher = $(options.catcher.id);
    var timer = $(options.time.id);
    var itemContainer = $(options.scene.id);
    //var itemContainer = $(options.scene.containerId);

    var containerWidth, itemWidth;
    var interval1, interval2;
    var timeout1;

    var keyDir = 0;
    var cursor = {x:0,y:0};
    var data = {
        time: 0,
        level: {
            segment: 0,
            current: 0
        },
        score: 000,
        session: {},
        game: []
    }

    init();

    function init() {
        
        for(var i=0; i<options.item.total-1; i++) {
       
            $(options.item.class+":first-child").clone().appendTo(options.item.containerId);

            if(options.effect != null) {
                $(options.effect.class+":first-child").clone().appendTo(options.effect.containerId);
            }
        }

        $(document).keydown(function(e) {
            if(e.keyCode == options.controls.left) keyDir = -1;
            else if(e.keyCode == options.controls.right) keyDir = 1;
        });
        $(document).keyup(function(e) { keyDir = 0; });

        data.level.segment = options.time.max / options.item.speed.length;
        
    }
    
    function start() {
        
        started = true;
        resetGame();
        //
        let p=$('.game-section .walter').offset();
        //$('#character').css({'position':'absolute','left':p.left,'top':p.top})
        $('#character').css({'position':'absolute','left':p.left,'bottom':'0vh'})
        $('.items').css({'display':'block'})
        $('.walter').css({'position':'relative'})
        $('.header').css({'z-index':'30'});
        
        //
        interval1 = setInterval(updateTime, 1000);
        interval2 = setInterval(updateFrame, options.fps);

        
        if(options.controls.enableMouse) {
            $(options.sensor.id).on('mousemove',updateCatcher);
        }
        $(options.sensor.id).on('touchmove',updateCatcher);

        data.session.start = Date.now();
    }

    function updateTime() {
        data.time++;

        data.level.current = Math.floor(data.time/data.level.segment);

        var remain = options.time.max - data.time;
        if(typeof timer !== 'undefined') timer.html(lpad(remain,2));

        if(data.time>=options.time.max) {
            finishGame();
        }
    }

    function updateCatcher(e) {
        e.preventDefault();
        e.stopPropagation();
        // catcher hidden at first
        //catcher.css({opacity:1});
        
        
        cursorActive = true;
        clearTimeout(timeout1);
        timeout1 = setTimeout(deactivateCursor,500);
        
        var _x = (e.originalEvent.touches != undefined) ? e.originalEvent.touches[0].clientX : e.pageX;
 
        cursor.x = _x - itemContainer.offset().left;
   
        if(options.catcher.moveVertical) {
            var _y = (e.originalEvent.touches != undefined) ? e.originalEvent.touches[0].clientY : e.pageY;
            cursor.y = _y - itemContainer.offset().top;
        }
        
        if(typeof options.callback.onCursorMove === 'function') options.callback.onCursorMove(cursor);
    }

    function deactivateCursor() {
        cursorActive = false;
    }

    function updateFrame() {

        for(var i=0; i<options.item.total; i++) {
            //update items position
            var item = $(options.item.class).eq(i);
            var itemX = parseInt(item.attr('data-x'));
            var itemY = parseInt(item.attr('data-y'));
            var speed = parseInt(item.attr('data-speed'));
            var newY = itemY + speed;
            item.attr('data-y',newY).css({top:newY+'px'});
            
            

            var hit1 = options.item.hit != null ? item.find(options.item.hit) : item;
            var hit2 = options.catcher.hit != null ? catcher.find(options.catcher.hit) : catcher;

            //check if item collide with catcher
            if(collision(hit1,hit2)) {
                //checking for "get into the catcher" logic
                //eg: packet, box, bucket, etc
                if(options.catcher.hasDepth) {
                    var catcherY = catcher.position().top - options.catcher.depthoffY;
                    if(newY>catcherY) continue; //dont count
                }
                
                var point = parseInt(item.attr('data-point'));
                
                if(data.score + point >= 0) {
                    
                    data.score += point;
                    var obj = {time:Date.now(),point:point};
                    if(point>0) data.game.push(obj);
                    else data.game.push(obj);

                    if(options.effect != null) showEffect(item,i);
                    if(options.score.id != null) $(options.score.id).html(lpad(data.score,3));
                    if(typeof options.callback.onScoreChanged == 'function') options.callback.onScoreChanged(data.score);
                    
                    // walter change
                    if(data.score >= 50 && data.score<100){
                        $('#walter-in-game').removeClass('walter-1 walter-3 walter-4').addClass('walter-2');
                    }else if(data.score >=100 && data.score<150){
                        $('#walter-in-game').removeClass('walter-1 walter-2 walter-4').addClass('walter-3');
                    }else if(data.score >=150){
                        $('#walter-in-game').removeClass('walter-1 walter-2 walter-3').addClass('walter-4');
                    }else{
                        $('#walter-in-game').removeClass('walter-2 walter-3 walter-4').addClass('walter-1');
                    }
                }
                
                resetItem(item);
            }
            
            //reset if item's position is out of scene
            var containerHeight = itemContainer.height();

            
            if(newY>containerHeight) resetItem(item);
        }

        var pos = {};
        if(cursorActive) {
            var offX = catcher.width()/2;
            var offY = catcher.height()/2;
            pos.x = cursor.x-offX;
            pos.y = cursor.y-offY;

        }
        else {
            var curX = catcher.position().left;
            pos.x = curX + (options.catcher.speed*keyDir);
        }
        
        let c= catcher.offset();

        let container_width=itemContainer.width();
        
        let itemWidth = $('#walter-in-game').width();
        itemWidth= 0-(itemWidth/2);

        let rightSide=container_width+itemWidth;
        
        
        if(pos.x< itemWidth || pos.x> rightSide){
            
        }else{
            catcher.css({left:pos.x});
        }
        
        
        if(options.catcher.moveVertical) catcher.css({top:cursor.y-offY});

        if(typeof options.callback.onUpdateFrame === 'function') options.callback.onUpdateFrame(pos);
    }

    function showEffect(item,index) {
        var effect = $(options.effect.class).eq(index);
        var fxIndex = arraySearch(options.effect.data, 'point', parseInt(item.attr('data-point')));
        console.log(fxIndex);
        var bgPos = options.effect.data[fxIndex].bgPos;
        var text = options.effect.data[fxIndex].text;

        effect.show().addClass('show '+options.effect.data[fxIndex].cssClass);
        effect.css({top:item.offset().top,left:item.offset().left});
        if(bgPos != null) effect.css({backgroundPosition:bgPos});
        else if(text != null) effect.html(text);

        setTimeout(function(){
            effect.hide().removeClass('show '+options.effect.data[fxIndex].cssClass);
        },1000);
    }

    function finishGame() {
        started = false;
        clearInterval(interval1);
        clearInterval(interval2);
        
        data.session.end = Date.now();

        if(typeof(options.callback.onFinish) === 'function') options.callback.onFinish(data);
    }

    function resetItem(item) {
        
        containerWidth = itemContainer.width();
        itemWidth = $(options.item.class).eq(0).width();
        var index = Math.floor(getRandomInRange(0, options.item.data.length-0.0001));
        var speed = Math.floor(getRandomInRange(options.item.speed[data.level.current].min, options.item.speed[data.level.current].max-0.0001));
        var x = Math.floor(getRandomInRange(10, containerWidth-(itemWidth*2)));
        var y = Math.floor(getRandomInRange(options.item.minY, -itemWidth));
        
        var _data = options.item.data[index];

        item.attr('data-point', _data.point);
        item.attr('data-speed', speed);
        item.attr('data-x', x);
        item.attr('data-y', y);

        item.css({top:y+'px',left:x+'px'}).removeClass().addClass(options.item.class.replace('.','')+' '+_data.cssClass);
    }

    function resetGame() {

        
        $('#walter-in-game').removeClass('walter-2 walter-3 walter-4').addClass('walter-1');
        
        clearInterval(interval1);
        clearInterval(interval2);
        started = false;
        data.time = 0;
        data.score = 0;
        data.level.current = 0;
        data.session = {};
        data.game = [];

        containerWidth = itemContainer.width();
        cursor.x = containerWidth/2;
        catcher.css({left:cursor.x - catcher.width()/2});

        $(options.score.id).html(lpad(data.score,3));

        for(var i=0; i<options.item.total; i++) {
            resetItem($(options.item.class).eq(i));
        }
        if(typeof timer !== 'undefined') timer.html(options.time.max);
        //catcher.css({opacity:0});
    }

    function restart() {
        resetGame();
        start();
    }

    //getters

    function getData() {
        return data;
    }

    /* utilities */

    function mergeObject(defaultObj, overrideObject, reference) {
		for (var attributeKey in overrideObject) {
			if (defaultObj.hasOwnProperty(attributeKey)) {
				defaultObj[attributeKey] = overrideObject[attributeKey];
			}
			else {
				console.warn('Key ['+attributeKey+'] not found in object merging process.', reference);
			}
		}

		return defaultObj;
    }

    function getRandomInRange(min, max) {
        return min + (max - min) * Math.random();
    }

    function collision(div1, div2) {
        var x1 = div1.offset().left;
        var y1 = div1.offset().top;
        var h1 = div1.outerHeight(true);
        var w1 = div1.outerWidth(true);
        var b1 = y1 + h1;
        var r1 = x1 + w1;
        var x2 = div2.offset().left;
        var y2 = div2.offset().top;
        var h2 = div2.outerHeight(true);
        var w2 = div2.outerWidth(true);
        var b2 = y2 + h2;
        var r2 = x2 + w2;
        var dValue = x1 + y1 + x2 + y2;
 
        if (b1 < y2 || y1 > b2 || r1 < x2 || x1 > r2 || dValue == 0) return false;
        return true;
    }

    function arraySearch(arr,key,val) {
        for (var i=0; i<arr.length; i++)
            //console.log(arr[i][key], val);
            if (arr[i][key] === val)                    
                return i;
        return false;
    }

    function lpad(value, padding) {
        var zeroes = new Array(padding+1).join("0");
        return (zeroes + value).slice(-padding);
    }

}