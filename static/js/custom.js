

$(function () { 
  var controller = new ScrollMagic();
  
  gsap.set(".product-col, .new-product-col", { y: "50" , opacity: 0});
  var tweenHistory = new TimelineMax()
    .add([
      gsap.to(".product-col-1", {
        y: "0",
        opacity: 1,
        delay: 0
      }),
      gsap.to(".product-col-2", {
        y: "0",
        opacity: 1,
        delay: 0.2
      }),
      gsap.to(".product-col-3", {
        y: "0",
        opacity: 1,
        delay: 0.4
      }),
      gsap.to(".product-col-4", {
        y: "0",
        opacity: 1,
        delay: 0.6
      }),
      gsap.to(".new-product-col", {
        y: "0",
        opacity: 1,
        delay: 0.8
      }),
    ]);
  var scene = new ScrollScene({triggerElement: '.history-section'})
    .setTween(tweenHistory)
    .addTo(controller);
});


/*home page - benefit slick*/ 
/*$('.benefit-slick').slick({
  arrows: true,
  lazyLoad: "progressive",
  dots: false,
  infinite: true,
  autoplay: true,
  autoplaySpeed: 8000,
  speed: 500,
  cssEase: 'ease'
}).slickAnimation();*/

/*home page - promotion slick*/ 
/*$('.promotion-slick').slick({
  arrows: true,
  lazyLoad: "progressive",
  dots: true,
  infinite: true,
  autoplay: true,
  autoplaySpeed: 8000,
  speed: 500,
  cssEase: 'ease'
}).slickAnimation();

$('.promotion-slick-mobile').slick({
  arrows: true,
  lazyLoad: "progressive",
  dots: true,
  infinite: true,
  autoplay: true,
  autoplaySpeed: 8000,
  speed: 500,
  cssEase: 'ease'
}).slickAnimation();
*/

/*home page - benefit slick with accodion mobile*/ 
var myCollapsible1 = document.getElementById('flush-collapseOne');
var myCollapsible2 = document.getElementById('flush-collapseTwo');
var myCollapsible3 = document.getElementById('flush-collapseThree');
$('.vs-slick-mobile').slick({
  arrows: true,
  lazyLoad: "progressive",
  slidesToShow: 1.1,
  dots: false,
  infinite: false,
  autoplay: true,
  autoplaySpeed: 8000,
  speed: 500,
  cssEase: 'ease'
});
myCollapsible1.addEventListener('shown.bs.collapse', function () {
  $('.vs-slick-mobile').slick({
    arrows: true,
    lazyLoad: "progressive",
    slidesToShow: 1.1,
    dots: false,
    infinite: false,
    autoplay: true,
    autoplaySpeed: 8000,
    speed: 500,
    cssEase: 'ease'
  });
})

myCollapsible2.addEventListener('shown.bs.collapse', function () {
  $('.benefit-slick-mobile').slick({
    arrows: true,
    lazyLoad: "progressive",
    dots: true,
    infinite: false,
    autoplay: true,
    autoplaySpeed: 8000,
    speed: 500,
    cssEase: 'ease'
  });
})

myCollapsible3.addEventListener('shown.bs.collapse', function () {
  $('.result-slick-mobile').slick({
    arrows: true,
    lazyLoad: "progressive",
    dots: true,
    infinite: false,
    autoplay: true,
    autoplaySpeed: 8000,
    speed: 500,
    cssEase: 'ease'
  });
})


$(".bt-result").click(function(e) {
  $(".result-slick-mobile").slick('slickNext'); 
});







$(document).ready(function () {
  $(window).scroll(function () {
    if ($(document).scrollTop() > 2) {
      $(".desktop-nav").addClass("desktop-nav1");
    } else {
      $(".desktop-nav").removeClass("desktop-nav1");
    }
  });
});

function openNav() {
  document.getElementById("mySidenav").style.width = "100%";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
}
