//let searchParams = new URLSearchParams(window.location.search)

// search param: email, code
var email = searchParams.get('email');
var code  = searchParams.get('code');

function getRedeemStatus() {

    $.ajax({
        type: 'GET',
        url: 'https://contenthub.innity-asia.com/2021/sg/laneige/staging/custom/get_redeem_status_action.php',
        data: {email: email},
        dataType: 'json',
        success: function (data) {
            if (data.code_status == 1) {
                $('a.btn-redeem').remove();
                $('.div-btn-redeem').append("<a class='btn-redeem solid-btn-theme2 font-calibri-bold font-20 text-white text-decoration-none rounded p-4 disabled'>Redeem Now</a>");
            }
        }
    });
}

function submitRedeem() {

    $('.btn-loading').show();
    $('#submit_redeem').prop('disabled', true);
    $('#submit_redeem').addClass('active');

    $.ajax({
        type: 'POST',
        url: 'https://contenthub.innity-asia.com/2021/sg/laneige/staging/custom/redemption_action.php',
        data: {email: email, code: code},
        dataType: 'json',
        success: function (data) {
            if (data.status == 1) {
                alert("Success !");
                $('.btn-redeem').addClass('disabled').css("background-color", "grey");
                $(".btn-redeem").prop("disabled",true);
                window.location.reload();
            } else {
                alert('Error :' + data.msg);
                $('.btn-loading').attr("style", "display: none!important");
                $('#submit_redeem').prop('disabled', false);
                $('#submit_redeem').removeClass('active');
            }
        }
    });

}

$(document).ready(function(){

    $('.redeem-code').html(code);
    getRedeemStatus();
});