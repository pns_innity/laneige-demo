/*!
 * VERSION: 1.0.1
 * DATE: 28/8/2019
 **/

 function JqForm(opts) {

    this.submit = submit;

    var options = mergeObject({
        url: null,
        targetId: null,
        autoSubmit: true,
        errorTextId: null,
        callback: {
            beforeSend: null,
            sending: null,
            complete: null,
            success:null,
            fail:null,
            exists:null
        }
    }, opts, 'options');

    if(typeof $ === 'undefined') {
        alert('Please include jquery script');
    }

    var formEl = $(options.targetId);
    var params = {};
    $(options.targetId).on('submit', validateForm);

    function validateForm(e) {

        //to prevent submission using form
        e.preventDefault();
        var hasEmpty = false;

        $(options.targetId+":input").each(function(){
            var input = $(this); // This is the jquery object of the input, do what you will
            if(hasEmpty) return;
            if((input.attr('type') == 'text' || input.attr('type') == 'email') && input.val() == '') {
                var name = input.attr('name');
                if(name != 'fbid') {
                    console.log(input);
                    alert('Please fill ' + name);
                    input.focus();
                    hasEmpty = true;
                }
            }
            else if(input.attr('type') == 'checkbox' && !input.prop('checked')) {
                alert('Please select ' + input.attr('name'));
                input.focus();
                hasEmpty = true;
            }
        });

        if(!hasEmpty) {
            constructParams();
            if(options.callback.beforeSend == null && options.autoSubmit) submit(params);
            else {
                if(typeof options.callback.beforeSend === 'function')
                    options.callback.beforeSend(params);
            }
        }
        
        
//        if($('#cb_tnc').is(":not(:checked)"))
//            {
//                alert("Please agree to Terms & Conditions.");
//                $('#cb_tnc').focus();
//                return false;
//            }

    }

    function constructParams() {
        params = objectifyForm(formEl.serializeArray());
        //params['url'] = getDocURL();
        
    }
   
    function submit($params) {

        // submit form using ajax because we dont want to change page
        var phpUrl = (formEl.attr('action') == undefined) ? options.url : formEl.attr('action');
        
        
        params['agree']= document.getElementById("policyCheckBox").checked;
        
        
        $.ajax({
            type: 'post',
            url: phpUrl,
            data: $params,
            dataType: 'json',
            complete: function ($data) {
                
                var data = JSON.parse($data.responseText);

                if (typeof (options.callback.complete) === 'function') {
                    options.callback.complete(data);
                }

                switch (data.status) {
                    case 0:
                    case "0":
                        if (typeof (options.callback.fail) === 'function') options.callback.fail(data);
                        showError(data);
                        break;

                    case 1:
                    case "1":
                        if (typeof (options.callback.success) === 'function') options.callback.success(data);
                        break;

                    case 2:
                    case "2":
                        if (typeof (options.callback.exists) === 'function') options.callback.exists(data);
                        showError(data);
                        break;
                }
            }
        });

        if (typeof (options.callback.sending) === 'function') options.callback.sending();
    }

    function showError(data) {
        
        if (options.errorTextId == null) {
            $('#err_msg').html(data.msg)
            
            // if(data.error != undefined) alert(data.error);
            // else alert('Something went wrong. Please try again later');
        }
        else $(options.errorTextId).html(data.error);
    }

    /* utilities */

    function mergeObject(defaultObj, overrideObject, reference) {
        for (var attributeKey in overrideObject) {
            if (defaultObj.hasOwnProperty(attributeKey)) {
                defaultObj[attributeKey] = overrideObject[attributeKey];
            }
            else {
                console.warn('Key [' + attributeKey + '] not found in object merging process.', reference);
            }
        }

        return defaultObj;
    }

    function objectifyForm(formArray) {//serialize data function
        var returnArray = {};
        for (var i = 0; i < formArray.length; i++) {
            if(formArray[i]['value'] != '')
                returnArray[formArray[i]['name']] = formArray[i]['value'];
            else {
                var _key = formArray[i]['name'];
                //for select tag
                returnArray[_key] = $("select[name='"+_key+"']:selected").text();
            }
        }
        delete returnArray.agree;

        return returnArray;
    }

    function getDocURL() {
        return window.location.href;
    }

}