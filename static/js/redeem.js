function validateContactIsNumber(number) {
    var re = /^[0-9]+$/;
    return re.test(number);
}

function validateContactDigit(number) {
    var re = /^[0-9]{8}$/;
    return re.test(number);
}

function validateEmail(email) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(email);
}

function isNumber(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

function isTextKey(evt){
    var regex = /^[a-zA-Z ]+$/;
    var str = String.fromCharCode(!evt.charCode ? evt.which : evt.charCode);
    if (!regex.test(str))
        return false;
    return true;
}

function updateNumberOfDays() {
    $('.valid_days').html('');
    month = $(".valid_months option:selected" ).data("month");
	year = 2021;
	days = daysInMonth(month, year);
    currentMonth = (new Date).getMonth() + 1;

    $('.valid_days').append("<option selected='selected' disabled class='grey-option'>Day</option>");

    if (month == currentMonth) {
        var today = new Date();
        var start = today.getDate() + 1;
    } else {
        var start = 1;
    }

	for (i = start; i < days + 1 ; i++) {
        $('.valid_days').append($('<option />').val(i).html(i));
	}
}

function daysInMonth(month, year) {
	return new Date(year, month, 0).getDate();
}


$(document).ready(function() {


    // hide January if current month is Febuary
    if ((new Date).getMonth() == 1) {
        $('.valid_months').html('');
        $('.valid_months').append('<option selected="selected" disabled class="grey-option text-center">Month</option>');
        $('.valid_months').append('<option value="February" data-month="2">February</option>');
    }

	$('.valid_months').change(function () {
		updateNumberOfDays();
	});


    $(document).on('submit', 'form#form_register', function (e) {
        e.preventDefault();

		if ($('.btn-submit').hasClass('active')) {
			return false;
		}

        $('.error-txt').each(function () {
            $(this).remove();
        });

        $('.hw_error').each(function () {
            $(this).removeClass('hw_error');
        });

        var name = $('input[name=name]').val().trim();
        var contact = $('input[name=contact]').val();
        var email = $('input[name=email]').val().trim();
        var store = $('select[name=store]').val();
        var month = $('select[name=months]').val();
        var day = $('select[name=days]').val();
        var member = $('select[name=member]').val();
        var tnc_agree = $('input[name=tnc_agree]').is(':checked');
        var prefer = [];
        $("input:checkbox[name=prefer]:checked").each(function() {
            prefer.push($(this).val());
        });

        var hvError = false;
        if (name == '') {
            $('.reg_name_wrapper').addClass('hw_error').append('<div class="error-txt" style="color: red;font-size: 15px;"> Please enter your Name</div>');
            hvError = true;
        }

        if (contact == '') {
            $('.reg_contact_wrapper').addClass('hw_error').append('<div class="error-txt" style="color: red;font-size: 15px;">Please enter your Contact Number</div>');
            hvError = true;
        } else {
            var validateContactNumberResult = validateContactIsNumber(contact);
            if (validateContactNumberResult !== true) {
                $('.reg_contact_wrapper').addClass('hw_error').append('<div class="error-txt" style="color: red;font-size: 15px;">This entry can only contain numbers</div>');
                hvError = true;
            } else {
				var validateContactDigitResult = validateContactDigit(contact);
				if (validateContactDigitResult !== true) {
					$('.reg_contact_wrapper').addClass('hw_error').append('<div class="error-txt" style="color: red;font-size: 15px;">Please enter a 8-digit Phone Number</div>');
					hvError = true;
				}
			}
        }

        if (email == '') {
            $('.reg_email_wrapper').addClass('hw_error').append('<div class="error-txt" style="color: red;font-size: 15px;">Please enter your Email</div>');
            hvError = true;
        } else {
            if (!validateEmail(email)) {
                $('.reg_email_wrapper').addClass('hw_error').append('<div class="error-txt" style="color: red;font-size: 15px;">Please enter your email address in format: yourname@example.com</div>');
                hvError = true;
            }
        }

		if (store == '' || store == null) {
            $('.reg_store_wrapper').addClass('hw_error').append('<div class="error-txt" style="color: red;font-size: 15px;">Please enter your Preferred Store</div>');
            hvError = true;
        }

		if (month == null || day == null) {
            $('.reg_date_wrapper').addClass('hw_error').append('<div class="error-txt" style="color: red;font-size: 15px;">Please choose the date</div>');
            hvError = true;
        }

        if (member == '' || member == null) {
            $('.reg_member_wrapper').addClass('hw_error').append('<div class="error-txt" style="color: red;font-size: 15px;">Please choose one</div>');
            hvError = true;
        }

		if (prefer.length == 0) {
            $('.reg_prefer_wrapper').addClass('hw_error').append('<div class="error-txt" style="color: red;font-size: 15px;">Please choose one</div>');
            hvError = true;
        }

        if (!tnc_agree) {
            $('.reg_agree_wrapper').addClass('hw_error').append('<div class="error-txt" style="color: red;font-size: 15px;">Please accept the Terms & Conditions</div>');
            hvError = true;
        }

        if (hvError) {
            return false;
        }

        var formData = {
            'name'		    : name,
            'contact'		: contact,
            'email'			: email,
            'store'        	: store,
            'month'      	: month,
            'day'      		: day,
            'member'        : member,
            'prefer'        : prefer.toString(),
            'agree'		    : tnc_agree,
			'url'           : window.location.href,
        };

        $('.btn-loading').show();
        $('.btn-sampling').prop('disabled', true);
		$('.btn-sampling').addClass('active');

        $.ajax({
            type: 'POST',
            url: 'https://contenthub.innity-asia.com/2021/sg/laneige/staging/custom/redemption_register_action.php',
            data: formData,
            dataType: 'json',
            success: function(data) {
                if (data.status == 1){
                    alert('Thank you for your participation.\n\nWe have sent you an email. Please present the email to our beauty advisors in-store to redeem your complimentary sampling kit.');
                    window.location.reload();
                } else if (data.status == 2) {
                    $('.reg_email_wrapper').addClass('hw_error').append('<div class="error-txt" style="color: red;font-size: 15px;">Email address already exists<br>Please use a different email address</div>');
                } else if (data.status == 3) {
					$('.reg_contact_wrapper').addClass('hw_error').append('<div class="error-txt" style="color: red;font-size: 15px;">Phone number already exists<br>Please use a different number</div>');
                } else {
					alert(data.status);
				}
                $('.btn-loading').attr("style", "display: none!important");
                $('.btn-sampling').prop('disabled', false);
				$('.btn-sampling').removeClass('active');
            },
            error: function(xhr,type) {
				console.log('error');
            }
        });
    });
});