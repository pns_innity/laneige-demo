<?php

class EngagePlus
{
    private $api_url = 'https://engageplus.innity-asia.com/api/0.2/';

    /**
     * The key & secret key
     * Must Define
     *
     * @var int
     */
    private $consumer_key = '';
    
    private $consumer_secret_key = '';
    
    private $endpoint = '';
    
    private $response = array();
    
    private $response_info = array();
    
    private $data = array();
    
    private $method = 'POST';
    
    private $authtype = 'backend';


    /**
     * The EngagePlus constructor
     *
     */
    public function __construct($consumer_key, $consumer_secret_key, $endpoint, $method = 'POST')
    {
        $this->consumer_key = is_null($consumer_key) ? '' : $consumer_key;
        $this->consumer_secret_key = is_null($consumer_secret_key) ? '' : $consumer_secret_key;
        $this->endpoint = is_null($endpoint) ? '' : $endpoint;
        $this->method = $method;
    }
    
    public function setAuthType($type)
    {
      $this->authtype = $type;
    }
    public function setApiUrl($api_url)
    {
      $this->api_url = $api_url;
    }
    
	public function getIp() 
	{
		global $_SERVER;
		$host  = $_SERVER["REMOTE_ADDR"];
		$proxy = false;
		if((isset($_SERVER["HTTP_VIA"]) && $_SERVER["HTTP_VIA"] != "") || (isset($_SERVER["HTTP_X_FORWARDED_FOR"]) && $_SERVER["HTTP_X_FORWARDED_FOR"] != "")) {
			$proxy = true;
		}
		if(!empty($_REQUEST['uip']))
		{
			$host = $_REQUEST['uip'];
		} elseif ($proxy) {
			if(isset($_SERVER["HTTP_FORWARDED"]) && $_SERVER["HTTP_FORWARDED"] != "") {
				$host = $_SERVER["HTTP_FORWARDED"];
			}
			if(isset($_SERVER["HTTP_FORWARDED_FOR"]) && $_SERVER["HTTP_FORWARDED_FOR"] != "") {
				$host = $_SERVER["HTTP_FORWARDED_FOR"];
			}
			if(isset($_SERVER["HTTP_X_FORWARDED"]) && $_SERVER["HTTP_X_FORWARDED"] != "") {
				$host = $_SERVER["HTTP_X_FORWARDED"];
			}
			if(isset($_SERVER["HTTP_X_FORWARDED_FOR"]) && $_SERVER["HTTP_X_FORWARDED_FOR"] != "") {
				$host = $_SERVER["HTTP_X_FORWARDED_FOR"];
			}
			if(isset($_SERVER["HTTP_CLIENT_IP"]) && $_SERVER["HTTP_CLIENT_IP"] != "") {
				$host = $_SERVER["HTTP_CLIENT_IP"];
			}
		}
		if(strpos($host, ",") !== FALSE) {
			$host = explode(",", $host);
			$host = trim($host[count($host) - 1]);
		}
		return $host;
	}
	
    public function postdata()
    {
	    $request_url = $this->endpointURL();
		$key = $this->consumer_key;
		$secret_key = $this->consumer_secret_key;
		$gmt = time();
		$md5_hash = md5($key.$request_url.$gmt.$secret_key);
		
		
		$fields_array['gmt'] = $gmt;
		$fields_array['key'] = $key;
		$fields_array['secret'] = $md5_hash;
		$fields_array['authtype'] = $this->authtype;
		$fields_array['getdmpjs'] = true;
		$fields_array['uagent'] = $_SERVER['HTTP_USER_AGENT'];
		$fields_array['uip'] = $this->getIp();
		
		$fields_array = array_merge($fields_array, $this->data);
		
		if($this->method == 'GET')
		{
			$fields_array = array_merge($fields_array, $this->data);
			$fields = '';
			foreach($fields_array as $field_key => $field_value)
			{
				$fields .= $field_key . '='.urlencode($field_value).'&';
			}
			
			$this->response = json_decode(@file_get_contents($request_url.'?'.$fields), true);
		}
		else
		{
			$ch = curl_init();
		
			curl_setopt($ch, CURLOPT_URL,$request_url);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_array);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			
			$response = curl_exec ($ch);
			$info = curl_getinfo($ch);
			$this->response_info = $info;
			curl_close ($ch);
			
			
			$this->response = json_decode($response, true);
			
		}
			
    }
    
    public function getScripts()
    {
	    if(!empty($this->response['script']))
	    {
		    return $this->response['script'];
	    }
	    else
	    {
		    return false;
	    }
    }
    
    public function getResponse()
    {
	    return $this->response;
    }
    
    public function getResponseInfo()
    {
	    return $this->response_info;
    }
    
    public function endpointURL()
    {
	    $url = $this->api_url . trim($this->endpoint, '/');
	    $ext = pathinfo($url, PATHINFO_EXTENSION);
	    if(empty($ext))
	    {
		    $url .= '.json';
	    }
	    else
	    {
		    $url = str_replace('.'.$ext, '.json');
	    }
	    return $url;
    }
    
    public function setData($param, $value)
    {
	    $this->data[$param] = $value;
    }
    
    public function getData($param, $value)
    {
	    return $this->data[$param];
    }

}

?>
