<?php

  header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');
  header('Access-Control-Allow-Credentials: true');
  header('Access-Control-Allow-Headers: Overwrite, Destination, Content-Type, Depth, User-Agent, Translate, Range, Content-Range, Timeout, X-File-Size, X-Requested-With, If-Modified-Since, X-File-Name, Cache-Control, Location, Lock-Token, If');
  header('Access-Control-Expose-Headers: DAV, content-length, Allow');
  if(@$_SERVER['HTTP_ORIGIN']){
    header("Access-Control-Allow-Origin: " . $_SERVER['HTTP_ORIGIN'] . "");
  } else {
    header('Access-Control-Allow-Origin: *');
  }
  
include('config.php');
include('EngagePlus_v1_1.php');

function get_data($name) {
  $data = '';
  if(isset($_POST[$name])){
    $data = $_POST[$name];
    unset($_POST[$name]);
  }
  return $data;
}

$endpoint  = get_data('endpoint');
$method    = strtoupper(get_data('method'));
$authtype  = get_data('authtype');
$token     = get_data('token');
$cid       = get_data('cid');
$timestamp = time();

$request   = new EngagePlus($engageplus_key, $engageplus_secret, $endpoint, $method);
$is_backend = array();
$is_backend[] = 'frontend';
$is_backend[] = 'backend';
$is_backend[] = 'read';
if (!in_array($authtype, $is_backend)) {
  $request->setAuthType($authtype);
}
if (isset($api_url)) {
  $request->setApiUrl($api_url);
}
$request->setData('cid', $cid);
$request->setData('token', $token);
$request->setData('timestamp', $timestamp);
foreach($_POST as $key => $value){
  $request->setData($key, $value);
}
$request->postData();
$response = $request->getResponse();
$info = $request->getResponseInfo();
echo json_encode($response);