var EngagePlusLoader = (function () {
	var my = {},
    config,
    loadedScript = [];
    
	function _mergeOptions(oldData, newData) {
		var latestData = {};
		if (typeof oldData !== 'undefined') {
			for (var attrname in oldData) {
				latestData[attrname] = oldData[attrname];
			}
		}
		if (typeof newData !== 'undefined') {
			for (var attrname in newData) {
				latestData[attrname] = newData[attrname];
			}
		}
		return latestData;
	}
  
  function _nextScript() {
    var firstScript = config.script.splice(0, 1)[0];
    if (typeof firstScript === 'undefined') {
      config.callback();
      return false;
    }
    my.loadScript(config.baseURL + config.scriptPath + config.ver + '/' + firstScript, _nextScript);
  }
  
  my.load = function (configuration) {
    config = {
      script: ['config.js', 'login.js' ,'api.js', 'widget.js'],
      ver: '2.0',
      baseURL: window.location.href.substring(0, window.location.href.lastIndexOf('/')+1),
      scriptPath: 'js/',
      callback: function () {}
    };
    config = _mergeOptions(config, configuration);
    _nextScript(config.script);
  };
  
  my.loadScript = function (scriptSrc, callback) {
		var index = scriptSrc.lastIndexOf('.') + 1,
		isCss = scriptSrc.substring(index, index + 3).toLowerCase() === 'css',
		protocol = scriptSrc.substring(0, scriptSrc.indexOf('://')).toLowerCase(),
		locally = protocol === '';
    if (typeof loadedScript[scriptSrc] !== 'undefined') {
      return ;
    }
    loadedScript[scriptSrc] = 1;
    if (isCss) {
      var g = document.createElement('link');
    } else {
      var g = document.createElement('script');
    }
    var s = document.getElementsByTagName('script')[0];
    if (isCss) {
      g.setAttribute('rel', 'stylesheet');
      g.setAttribute('type', 'text/css');
      if (locally) {
        g.setAttribute('href', baseUrl + scriptSrc);
      } else {
        g.setAttribute('href', scriptSrc);
      }
    } else {
      if (locally) {
        g.src = baseUrl + scriptSrc;
      } else {
        g.src = scriptSrc;
      }
      console.log(g.src);
    }
    if (typeof callback === 'function') {
      g.onload = callback;
    }
    s.parentNode.insertBefore(g, s);
    
	};
	return my;
}());
