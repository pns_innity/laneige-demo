var engageplus_cid = 309;
var engageplusCampaignId = engageplus_cid;
var engageplusUid = 0;
var engageplusToken = 'empty';
var GAME_CODE = {
    LIKE: 'like_on_post',
    SHARE: 'shared_post',
    COMMENT: 'commented_on_post',
    LOGIN: 'login',
    SHARE_SITE: 'share_content_hub',
}
var n10nAddList = [];
var n10nLeaveList = [];
var n10nDeleteList = [];
function gamification_login_callback(obj) {
    Gamification.userLogin(obj);
    if (!obj.uid) {
        return;
    }
    engageplusUid = obj.uid;
    var cid = engageplusCampaignId;
    var token = EngagePlusLogin[cid].getToken();
    engageplusToken = token;
    var user = Gamification.getAPI().user();
    if (typeof (Gamification) != 'undefined' && typeof (GAME_CODE) != 'undefined') {
        Gamification.getAPI().gamificationAction(GAME_CODE.LOGIN);
    }
    if (typeof engageplus_login_callback !== 'undefined') {
        engageplus_login_callback(obj);
    }
    game_callback_triggered = true;

}

EngagePlusLoader.load({
    baseURL: 'https://eplus.innity-asia.com/v2/' + engageplus_cid + '/',
    scriptPath: 'js/',
    callback: loadGamification
});

function showNotification(data) {
    var n10nDelayMS = 500;
    for (var i = 0; i < data.achievement.length; i++) {
        var obj = data.achievement[i];
        var n10nId = Gamification.getAPI().user().n10nId;
        if (!n10nId) {
            n10nId = 1;
        } else {
            n10nId++;
        }
        Gamification.getAPI().user({n10nId:n10nId});
        data.id = n10nId;
        switch (obj.type) {
            case 'badge':
                data.message = 'You\'ve obtained "' + obj.name + '"';
                break;
            default :
                data.message = obj.text;
        }
        notificationHTML = document.getElementById('notification').innerHTML;
        n10nAddList.push(JSON.parse(JSON.stringify(data)));
        n10nLeaveList.push(n10nId);
        setTimeout(function() {
            var data = n10nAddList.shift();
            $('#notification').prepend(Mustache.render(Gamification.getTemplate('notification'), data));
        }, 0 + (i * n10nDelayMS));
        setTimeout(function() {
            var n10nId = n10nLeaveList.shift();
            removeNotification(document.getElementById('notification_' + n10nId));
        }, 1500 + (i * n10nDelayMS));
    }
}
function removeNotification(elm) {
    if (!elm) {
        return ;
    }

    $(elm).removeClass('show slideup');
    elm.className += ' slidedown ';
    n10nDeleteList.push(elm);
    setTimeout(function() {
        var elm = n10nDeleteList.shift();
        if (elm && elm.parentNode) {
            elm.parentNode.removeChild(elm);
        }
    }, 2000);
}

function refresh() {
    engageplusUid = 0;
    engageplusToken = 'empty';
    if (typeof user_logout !== 'undefined') {
        user_logout();
    }
    setTimeout(function () {
        //location.href = 'index.php';
        location.reload();
    }, 2000);
}

function loadGamification() {
    if (typeof EngagePlusLogin === 'undefined') {
        EngagePlusLogin = {};
    }
    EngagePlus.login_action = EngagePlus.loginAction;
    EngagePlusLogin[engageplusCampaignId] = EngagePlus;
    EngagePlusLogin[engageplusCampaignId].login('EngagePlusLogin[' + engageplusCampaignId + ']', {
        cid: engageplusCampaignId,
        'providers': ['Facebook', 'Twitter', 'Google', 'LinkedIn'],
        loginCallback: gamification_login_callback,
        noPopup: true,
        callbackUrl: window.location.href,
        containerId: 'engageplus-login-root'
    });
    Gamification = EngagePlusWidget;

    Gamification.data('Gamification', {
        login: EngagePlusLogin[engageplusCampaignId],
        campaignId: engageplusCampaignId,
        withFanout: true,
        loadOnDemand: true,
        showUserPanel: true,
        showSplashTab: true,
        myActivityCallback: function (data) {
            var notificationHTML = '';
            if (document.getElementById('total_point')) {
                document.getElementById('total_point').innerHTML = data.points;
            }
            if (!data.achievement) {
                return ;
            }
            showNotification(data);
        },
        logoutCallback: function () {
            refresh();
        },
        sessionEndCallback: function () {
            refresh();
        }
    });

    Gamification.gamificationAction = Gamification.getAPI().gamificationAction;
    Gamification.user = Gamification.getAPI().user;
    Gamification.config = {
        campaign_id: engageplusCampaignId
    };
}
