var EngagePlusAPI = (function () {
	var my = {},
		config = [],
		instanceName,
		baseUrl = typeof base_url !== 'undefined' ? base_url : window.location.href.substring(0, window.location.href.lastIndexOf('/')+1), //note: base url from socialhub config
		gameConfig,
		rank = -1,
		allActivity,
		myActivity,
		leaderboardData = [],
		actionList = [],
		userData = {},
		activityData = [];
		activityData[0] = [],
		activityData[1] = [];
		fanoutLoaded = false,
		activityData[0].activity = [],
		activityData[1].activity = [],
		actionQue = [];

	function _processActivity(data) {
		if (data.activity) {
			for (var i = 0; i < data.activity.length; i++) {
				var activity = data.activity[i];
				var duration = _getDisplayTime(activity.timestamp, data.requestTime);
				data.activity[i].time = duration.text;
				data.activity[i].localeTime = duration.time;
				if (activity.uid == my.user().uid) {
					if (!activity.img) {
						data.activity[i].img = my.user().thumbnailURL;
					}
					data.activity[i].message = activity.message.replace(my.user().name, 'You');
				}
			}
		}
		return data;
	}

	function _mergeOptions(oldData, newData) {
		var latestData = {};
		if (typeof oldData !== 'undefined') {
			for (var attrname in oldData) {
				latestData[attrname] = oldData[attrname];
			}
		}
		if (typeof newData !== 'undefined') {
			for (var attrname in newData) {
				latestData[attrname] = newData[attrname];
			}
		}
		return latestData;
	}

	function _getDisplayTime(time, requestTime) {
		if (!requestTime) {
			requestTime = _now();
		} else {
			requestTime = requestTime * 1000;
		}
		var duration = requestTime - new Date(time * 1000),
		result = [],
		aDay = 3600 * 24;
		aMonth = aDay * 31;
		aYear = aMonth * 12;
		result.time = _now() - duration;
		result.time /= 1000;
		duration /= 1000;
		if (duration < 45) {
			result.text = 'less than a minute';
		} else if (duration < 100) {
			result.text = 'about a minute';
		} else if (duration < 3500) {
			result.text = _floorHack(duration / 60) + ' minutes';
		} else if (duration < (3600 * 2)) {
			result.text = 'about an hour';
		} else if (duration < aDay) {
			result.text = 'about ' + _floorHack(duration / 3600) + ' hours';
		} else if (duration < (aDay * 2)) {
			result.text = 'a day';
		} else if (duration < aMonth) {
			result.text = _floorHack(duration / aDay) + ' days';
		} else if (duration < (aMonth * 2)) {
			result.text = 'about a month';
		} else if (duration < aYear) {
			result.text = _floorHack(duration / aMonth) + ' months';
		} else if (duration < (aYear * 2)) {
			result.text = 'about a year';
		} else {
			result.text = _floorHack(duration / aYear) + ' years';
		}
		return result;
	}

	function _now(unixFormat) {
		var timestamp = _roundHack(new Date() / 1000);
		if (unixFormat) {
			return timestamp;
		} else {
			return timestamp * 1000;
		}
	}

	function _floorHack(number) {
		return number << 0;
	}

	function _roundHack(number) {
		return (0.5 + number) << 0;
	}

	function _toCamel(o) {
		var build, key, destKey, value;
		if (o instanceof Array) {
			build = [];
			for (key in o) {
				value = o[key];
				if (typeof value === "object") {
					value = _toCamel(value);
				}
				build[key] = value;
			}
		} else if (o instanceof Object) {
		build = {};
			for (key in o) {
				if (o.hasOwnProperty(key)) {
					destKey = key.replace(/_([a-z])/g, function (g) { return g[1].toUpperCase(); });
					value = o[key];
					if (value !== null && typeof value === "object") {
						value = _toCamel(value);
					}
					build[destKey] = value;
				}
			}
		} else {
			return o;
		}
		return build;
	}

	/* Set / Get data */
	my.data = function (name, configuration){
		instanceName = name;
		config = {
			showLoginPanel : true,
			showSplashTab : false,
			showUserPanel : false,
			withFanout : true,
			showGlobalChannel : false,
			allActivityCallback : function (obj) {} ,
			myActivityCallback : function (obj) {} ,
			campaignId : '', // required
			apiKey : '', // required
			uid : 0
		};
		config = _mergeOptions(config, configuration);
	};
	/* End Set / Get data */

	my.get = function (url, data, callback, sync) {
		var query = [];
		for (var key in data) {
			query.push(encodeURIComponent(key) + '=' + encodeURIComponent(data[key]));
		}
		my.send(url + '?' + query.join('&'), callback, 'GET', null, sync);
	};

	my.post = function (url, data, callback, sync) {
		var query = [];
		for (var key in data) {
			query.push(encodeURIComponent(key) + '=' + encodeURIComponent(data[key]));
		}
		my.send(url, callback, 'POST', query.join('&'), sync);
	};

	my.api = function (apiUrl, params, method, authtype, callback, sync, skip) {
		if (config.uid === 0 && authtype !== 'read' && authtype !== 'session') {
			if (typeof skip === 'undefined') {
				var funcParams = [apiUrl, params, method, authtype, callback, sync, 1];
				var func = my.addQueue(my.api, this, funcParams);
				actionQue.push(func);
			}
		} else {
			if (authtype === null) {
				authtype = 'frontend';
			}
			authtype = authtype.toLowerCase();
			var data = {
				cid : config.campaignId,
				endpoint : apiUrl,
				method : method,
				authtype : authtype
			};
			data = _mergeOptions(data, params);
			if (apiUrl === '/gamification/action') {
				var extraParams = {
					uid : config.uid,
					hidden_data : 'From JS cid: ' + config.campaignId,
					token : config.apiToken
				};
				data = _mergeOptions(data, extraParams);
			}
			if (typeof callback === 'undefined') {
				callback = function () {};
			}
			my.post(baseUrl+'/gamification/submit.php', data, callback, sync);
		}
	};

	my.activity = function (params, callbackFunction) {
		if (typeof callbackFunction !== 'function') {
			callbackFunction = function (obj) {return obj;};
		}
		var index =	+ (typeof params !== 'undefined');
        var limit = 25;
        if (params.limit) {
            limit = params.limit;
        }
		my.api('/activity/' + limit, params, 'get', 'read', function (data) {
			activityData[index] = _processActivity(JSON.parse(data));
			var displayData = [];
			displayData = activityData[index];
			return callbackFunction(_toCamel(displayData));
		}, false);
		return callbackFunction(_toCamel(JSON.parse(JSON.stringify(activityData[index]))));
	};

	my.closeFanout = function () {
		fanoutLoaded = false;
		if (myActivity) {
			myActivity.cancel();
		}
		if (allActivity) {
			allActivity.cancel();
		}
	};

	my.userRank = function (uid, callbackFunction, preload) {
		if (typeof callbackFunction !== 'function') {
			callbackFunction = function (obj) {return obj;};
		}
		if (rank != -1) {
			return callbackFunction(_toCamel(rank));
		}
		if (!uid) {
			uid = my.user().uid || config.uid;
		}
		my.api('/leaderboard/rank', {
			uid : uid
		}, 'post', 'read', function (data) {
			data = JSON.parse(data);
			rank = data[0];

			return callbackFunction(_toCamel(rank));
		}, preload);
		return callbackFunction(_toCamel(rank));
	};

	my.userMission = function (uid, callbackFunction) {
		if (typeof callbackFunction !== 'function') {
			callbackFunction = function (obj) {return obj;};
		}
		if (!uid) {
			uid = my.user().uid || config.uid;
		}
        var mission = [];
		my.api('/user/' + uid + '/mission', {
			uid : uid
		}, 'get', 'read', function (data) {
			data = JSON.parse(data);
			mission = data['mission'];
			return callbackFunction(_toCamel(mission));
		}, false);
		return callbackFunction(_toCamel(mission));
	};

	my.userBadge = function (uid, callbackFunction) {
		if (typeof callbackFunction !== 'function') {
			callbackFunction = function (obj) {return obj;};
		}
		if (!uid) {
			uid = my.user().uid || config.uid;
		}
        var badge = [];
		my.api('/user/' + uid + '/badge', {
			uid : uid
		}, 'get', 'read', function (data) {
			data = JSON.parse(data);
			badge = data['badge'];

			return callbackFunction(_toCamel(badge));
		}, false);
		return callbackFunction(_toCamel(badge));
	};

	my.leaderboard = function (limit, callbackFunction, preload) {
		if (typeof callbackFunction !== 'function') {
			callbackFunction = function (obj) {return obj;};
		}
		var DEFAULT_LIMIT = 10;
		if (!limit) {
			limit = DEFAULT_LIMIT;
		}
		limit *= 1;
		if (limit < 1) {
			limit = DEFAULT_LIMIT;
		}
		if (leaderboardData && leaderboardData[limit]) {
			return callbackFunction(_toCamel(leaderboardData[limit]));
		}
		my.api('/leaderboard/' + limit, null, 'get', 'read', function (data) {
			leaderboardData[limit] = JSON.parse(data);
			return callbackFunction(_toCamel(leaderboardData[limit]));
		}, preload);
		return callbackFunction(_toCamel(leaderboardData[limit]));
	};

        my.store = function (callbackFunction, preload) {
            if (typeof callbackFunction !== 'function') {
                callbackFunction = function (obj) {
                    return obj;
                };
            }
            my.api('/product/listing', {e: 1}, 'get', 'read', function (data) {
                productData = JSON.parse(data);
                return callbackFunction(_toCamel(productData));
            }, false);
            return callbackFunction(_toCamel(productData));
        };

        my.redeem = function (productId, callbackFunction, preload) {
            if (typeof callbackFunction !== 'function') {
                callbackFunction = function (obj) {
                    return obj;
                };
            }
            var uid = my.user().uid || config.uid;
            my.api('/redemption/' + productId + '/redeem', {
                uid: uid
            }, 'post', 'read', function (data) {
                var data = JSON.parse(data);
                return callbackFunction(_toCamel(data));
            }, true);
        };

	my.gamificationConfig = function (callbackFunction, preload) {
		if (typeof callbackFunction !== 'function') {
			callbackFunction = function (obj) {return obj;};
		}
		if (gameConfig) {
			return callbackFunction(_toCamel(gameConfig));
		}
		my.api('/gamification/config', null, 'post', 'read', function (data) {
			gameConfig = JSON.parse(data);

			if (gameConfig.badge) {
				var pointArr = [];
				for (i = 0; i < gameConfig.bonus.length; i++) {
					var key = JSON.stringify(gameConfig.bonus[i].action);
					pointArr[key] = [];
					pointArr[key].push(gameConfig.bonus[i].points);
				}
				for (i = 0; i < gameConfig.badge.length; i++) {
					var key = JSON.stringify(gameConfig.badge[i].action);
					if (pointArr[key]) {
						var pointVal = pointArr[key].shift();
						gameConfig.badge[i].points = pointVal;
					} else {
						gameConfig.badge[i].points = 0;
					}
				}
				return callbackFunction(_toCamel(gameConfig));
			}
		}, preload);
		return callbackFunction(_toCamel(gameConfig));
	};

	my.gamificationBadge = function (callbackFunction, preload) {
		if (typeof callbackFunction !== 'function') {
			callbackFunction = function (obj) {return obj;};
		}
		var gameBadge = [];
		my.api('/gamification/badge', { uid: my.user().uid }, 'get', 'read', function (data) {
			gameBadge = JSON.parse(data);
			return callbackFunction(_toCamel(gameBadge));
		}, false);
		return callbackFunction(_toCamel(gameBadge));
	};

	my.gamificationAction = function (actionCode, extraInfo, callbackFunction) {
		if (typeof extraInfo === 'undefined') {
			extraInfo = {};
		}
		if (typeof callbackFunction !== 'function') {
			callbackFunction = function (obj) {return obj;};
		}
		my.api('/gamification/action', {
			action_code : actionCode,
			args : JSON.stringify(extraInfo),
			result : -1
		}, 'post', 'frontend', function (data) {
			data = JSON.parse(data);
			return callbackFunction(_toCamel(data));
		}, true);
	};

	my.send = function (url, callback, method, data, sync) {
		var x = my.x();
		if (typeof sync === 'undefined') {
			sync = false;
		}
		x.open(method, url, sync);
		x.onreadystatechange = function () {
			if (x.readyState === 4) {
				callback(x.responseText);
			}
		};
		if (method === 'POST') {
			x.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
		}
		x.send(data);
	};

	my.addQueue = function (fn, context, params) {
		return function () {
			fn.apply(context, params);
		};
	};

	my.user = function (data, clearData) {
		var points = 0;
		if (clearData) {
			userData = {};
			config.uid = 0;
			config.apiToken = '';
		}
		userData['instance_name'] = instanceName;
		userData['theme_path'] = baseUrl + 'gamification/template/';
		if (data) {
			if (data.new_badge) {
				if (!userData.badges) {
					userData.badges = [];
				}
				data.new_badge = data.new_badge.replace(/^https:/gi, "http:");
				userData.badges.push(data.new_badge);
				return _toCamel(userData);
			}
			if (data.points) {
				points = userData.points;
			}
			userData = _mergeOptions(userData, data);
			if (points > userData.points) {
				userData.points = points;
			}
		}
		if (userData.uid) {
			config.uid = userData.uid * 1;
		}
		if (userData.apiToken) {
			config.apiToken = userData.apiToken;
		}
		if (!userData.cash) {
			userData.cash = 0;
		}
		if (!fanoutLoaded) {
			fanoutLoaded = my.loadFanout();
		}
		return _toCamel(userData);
	};

	my.loadScript = function (scriptId, scriptSrc) {
		var index = scriptSrc.lastIndexOf('.') + 1,
		isCss = scriptSrc.substring(index, index + 3).toLowerCase() === 'css',
		protocol = scriptSrc.substring(0, scriptSrc.indexOf('://')).toLowerCase(),
		locally = protocol === '';
		if ( document.getElementById(scriptId) === null) {
			if (isCss) {
				var g = document.createElement('link');
			} else {
				var g = document.createElement('script');
			}
			var s = document.getElementsByTagName('script')[0];
			if (isCss) {
				g.setAttribute('rel', 'stylesheet');
				g.setAttribute('type', 'text/css');
				if (locally) {
					g.setAttribute('href', baseUrl + scriptSrc);
				} else {
					g.setAttribute('href', scriptSrc);
				}
			} else {
				if (locally) {
					g.src = baseUrl + scriptSrc;
				} else {
					g.src = scriptSrc;
				}
			}
			g.setAttribute('id', scriptId);
			s.parentNode.insertBefore(g, s);
		}
	};

	my.x = function () {
		if (typeof XMLHttpRequest !== 'undefined') {
			return new XMLHttpRequest();
		}
		var versions = [
			'MSXML2.XmlHttp.5.0',
			'MSXML2.XmlHttp.4.0',
			'MSXML2.XmlHttp.3.0',
			'MSXML2.XmlHttp.2.0',
			'Microsoft.XmlHttp'
		];
		var xhr;
		for (var i = 0; i < versions.length; i++) {
			try {
				xhr = new ActiveXObject(versions[i]);
				break;
			} catch (e) {}
		}
		return xhr;
	};

	my.onLoadFunction = function() {
		if (!fanoutLoaded) {
			fanoutLoaded = my.loadFanout();
		}
	};

	my.loadFanout = function() {
		fanoutLoaded = true;
		if (!my.user().uid) {
			return false;
		}
		if (actionQue.length > 0) {
			for (var i = 0; i < actionQue.length; i++) {
				setTimeout(function () {
					var queueFunc = actionQue.shift();
					if (typeof queueFunc === 'function') {
						(queueFunc)();
					}
				}, 300 * i);
			}
		}
		if (config.withFanout) {
			if (typeof Faye === 'undefined') {
				return false;
			}

			var client = new Faye.Client('https://9c46e09b.fanoutcdn.com/bayeux');

			if (config.showGlobalChannel) {
				allActivity = client.subscribe('/campaign_' + config.campaignId, function (data) {
					// data = JSON for all users activities
					config.allActivityCallback(_toCamel(data));
				}); //end allActivity
			}
			myActivity = client.subscribe('/campaign_' + config.campaignId + '_' + my.user().uid, function (data) {
				// data = JSON for My activities / optained badges / missions / etc..
				try {
					var json = JSON.parse(data);
				} catch (e) {
					var json = data;
				}
				var currentTime = _now(true);

				if (json.error_desc) {
					json.message = json.error_desc;
					json.error = true;
				}
				if (!json.achievement || json.achievement.length === 0) {
					json.achievement = [];
				}
				json.achievement.unshift({
					'text'	: json.message,
					'point' : json.point_earned
				});
				if (json.achievement) {
					if (json.point_unit) {
						var point_unit = json.point_unit.split('&');
					} else if (my.gamificationConfig().pointUnit) {
						var point_unit = my.gamificationConfig().pointUnit.split('&');
					} else {
						var point_unit = my.gamificationConfig().point_unit.split('&');
					}
					var point_word	 = point_unit,
					currentLevel	 = '';
					if (json.current_level && json.current_level !== '') {
						currentLevel = json.current_level;
					}
					json.newAchievement = [];
					for (var i = 0; i < json.achievement.length; i++) {
						var obj				 = json.achievement[i];
						obj['uid']			= json.uid;
						obj['username'] = json.username;
						obj['user_img'] = json.user_img;
						if (json.current_level && json.current_level !== '') {
							currentLevel = json.current_level;
						}
                                                if (!obj['message']) {
                                                    obj['message']	= json.message;
                                                }
						obj['point_earned']	= obj['point'];
						obj['instance_name'] = instanceName;
						if (obj['point'] > 1) {
							obj['point_unit'] = point_unit[1].trim();
						} else {
							obj['point_unit'] = point_unit[0].trim();
						}
						if (obj['text']) {
							obj['text'] = obj['text'].replace(obj['username'], 'You');
						}
						if (obj.type != 'level' && obj.type != 'badge' && obj.type != 'mission') {
							obj['now'] = currentTime;
						}
						if (actionList.indexOf(JSON.stringify(obj)) > -1) {
							continue;
						} else {
						if (actionList.length === 20) {
							actionList.shift();
						}
							actionList.push(JSON.stringify(obj));
						}
						switch (obj.type) {
							case 'badge':
								my.user({'new_badge' : obj.img});
							break;
							case 'mission':
							case 'level':
							default:
							if (obj.type == 'level' && currentLevel == '') {
								currentLevel = obj.name;
							}
							break;
						}//end switch
						json.newAchievement.push(obj);
					}//end for
					if (point_unit[0]) {
						my.user({'point_unit' : point_unit[0].trim()});
					}
					if (json.total_points) {
						my.user({'points' : json.total_points});
					}
					if (currentLevel != '') {
						my.user({'current_level' : currentLevel});
					}
				}//end achievement
				json.achievement = json.newAchievement;
				delete json.newAchievement;
				config.myActivityCallback(_toCamel(json));
			});//end myActivity
			return true;
		}else{
			return false;
		}
	}

	my.loadScript('fanout_js', 'https://9c46e09b.fanoutcdn.com/bayeux/static/faye-browser-min.js');

	return my;
}());

if(window.addEventListener){
	window.addEventListener('load', EngagePlusAPI.onLoadFunction);
}else{
	window.attachEvent('onload', EngagePlusAPI.onLoadFunction);
}