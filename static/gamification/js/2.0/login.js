var EngagePlus = (function () {
	var my = {},
		cacheData = [],
		currentToken = "",
		instanceName,
		ua = navigator.userAgent.toLowerCase(),
		isSafari = ua.indexOf('safari') != -1 && !(ua.indexOf('chrome') > -1) && !(ua.indexOf('crios') > -1),
		protocol = "https://",
		inited = false,
		userData = {},
		popupWindow = null,
		closeWindow = null,
		token = null,
		monitorInterval = null,
		synchronousConfig = [],
		monitorClose = false,
		config;

	/* Set / Get variable data */
	function _cacheData(name, value) {
		if (value !== undefined && value !== null){
			cacheData[name] = value;
		}
		return cacheData[name];
	}
	/* End Set / Get variable data */

	/* Check expired local storage*/
	function _checkLocalStorage(name){
		try{
			var data = JSON.parse(localStorage.getItem(name));
			var keyValue = data.value;
			var timestamp = data.timestamp;
			if (timestamp){
				var now = Math.round(new Date().getTime()/1000);
				if ( now > timestamp ){
					localStorage.removeItem(name);
					_cacheData(name, null);
				}
			}
		}catch(e){
		}
	}
	/* End Check expired local storage*/

	function _checkSession() {
		if(currentToken !== '' && currentToken !== my.getToken() && typeof(userData.uid) !== 'undefined' && userData.uid) {
			currentToken = '';
			if (typeof(config.sessionEndCallback) != 'undefined') {
				config.sessionEndCallback();
			}
			my.logout();
		}
		if(currentToken !== '') {
			setTimeout(function() {
				_checkSession();
			}, 3000);
		}
	}

	/* Set / Get cookies */
	function _cookie(cName, value, exDays) {
		if (cName === undefined){
			return;
		}
		if (value !== undefined){
			var exDate = new Date();
			if(typeof exDays === "number"){
				exDate.setDate(exDate.getDate() + exDays);
			}
			var cValue = escape(value) + ((exDays == null) ? ';' : '; expires=' + exDate.toUTCString() + ';') + ' path=/';
			document.cookie = cName + '=' + cValue;
		}
		var cValue = document.cookie;
		var cStart = cValue.lastIndexOf(' ' + cName + '=');
		if (cStart == -1) {
			cStart = cValue.lastIndexOf(cName + '=');
		}
		if (cStart == -1) {
			cValue = null;
		} else {
			cStart = cValue.indexOf('=', cStart) + 1;
			var cEnd = cValue.indexOf(';', cStart);
			if (cEnd == -1) {
				cEnd = cValue.length;
			}
			cValue = unescape(cValue.substring(cStart, cEnd));
		}
		return cValue;
	};
	/* End Set / Get cookies */

	/* Cookie Alert */
	function _cookieAlert(){
		alert("Please enable cookies of your browser");
	}
	/* End Cookie Alert */

	/* Check if cookie available */
	function _cookieAvailable(){
		var cookieEnabled;
		document.cookie="test_cookie=test;Path=/; ";
		cookieEnabled=(document.cookie.indexOf("test_cookie")!=-1)? true : false;
		document.cookie = 'test_cookie=;Path=/; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
		if (cookieEnabled){
			return true;
		}
		return false;
	}
	/* End Check if cookie available */

	function _data(name, value, exDays, internal) {
		// internal 1 = cookie
		// internal 0 = local storage
		var useCookie = internal == 1 || _isIos() || !_lsAvailable();
		try {
			if (value !== undefined) {
				_cacheData(name, value);
				if (useCookie) {
					_cookie(name, value, exDays);
				} else {
					_localStorage(name, value, exDays);
				}
			}
			if (_cacheData(name) && !internal) {
				return _cacheData(name);
			}
			if (_cookie(name)) {
				return _cookie(name);
			} else {
				_checkLocalStorage(name);
				return _localStorage(name);
			}
		} catch (e) {
			return null;
		}
	}

	/* Set / Get local storage */
	function _localStorage(lsName, value, exDays) {
		if (lsName === undefined){
			return;
		}
		lsName = lsName.trim();
		if(exDays){
			var exDate = new Date();
			if(typeof exDays === "number"){
				exDate.setDate(exDate.getDate() + exDays);
			}
			var object = {value: value, timestamp: Math.round(exDate/1000) };
		}else{
			var object = {value: value};
		}
		if (value !== undefined){
			value = escape(value);
			localStorage.setItem(lsName, JSON.stringify(object));
		}
		value = localStorage.getItem(lsName);
		try{
			var data = JSON.parse(value);
			var keyValue = data.value;
			var timestamp = data.timestamp;
			if (keyValue) {
				return keyValue;
			} else {
				return value;
			}
		}catch(e){
			return value;
		}
	}

	/* Check if local storage available */
	function _lsAvailable(){
		var test = 'test';
		try {
			localStorage.setItem(test, test);
			localStorage.removeItem(test);
			return true;
		} catch(e) {
			return false;
		}
	}
	/* End Check if local storage available */

	/* End Set / Get local storage */

	function _mergeOptions(oldData, newData) {
		var latestData = {};
		if (typeof oldData !== 'undefined') {
			for (var attrName in oldData) {
				latestData[attrName] = oldData[attrName];
			}
		}
		if (typeof newData !== 'undefined') {
			for (var attrName in newData) {
				latestData[attrName] = newData[attrName];
			}
		}
		return latestData;
	}

	/* Detect IOS	*/
	function _isIos() {
		return /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
	};
	/* End Detect IOS	*/

        /* Fire Pixel */
        function _firePixel(uparams) {
		var oImg = document.createElement("img");
                oImg.setAttribute('src', '//avd-ep.innity.com/unified_profile/pixel_new.php?' + uparams);
		oImg.setAttribute('alt', '');
		oImg.setAttribute('height', '1px');
		oImg.setAttribute('width', '1px');
		document.body.appendChild(oImg);
	}
        /* End Fire Pixel */

    function rot13(str) {
        var input = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.split('');
        var output = 'NOPQRSTUVWXYZABCDEFGHIJKLMnopqrstuvwxyzabcdefghijklm'.split('');
        var lookup = input.reduce((m, k, i) => Object.assign(m, {
                [k]: output[i]
            }), {});
        return str.split('').map(x => lookup[x] || x).join('');
    }

	my.templates = {};

	/* Set / Get data */
	my.data = function (name, value, exDays){
		if (value !== undefined){
			_data(name, value, exDays);
		}
		return _data(name, value, exDays);
	}

	/* End Set / Get data */


	/* Engageplus Login */

	my.action = function (paramObj) {
		var paramsData = '';
		if (typeof(paramObj.params) != 'undefined') {
			paramsData = my.serialize(paramObj.params);
			if (paramsData) {
				paramsData = '&' + paramsData;
			}
		}
		if (typeof(config.actionParam) != 'undefined') {
			paramsData += '&actionParam=' + config.actionParam;
		}
		paramsData = '?token=' + my.getToken();
		paramsData += '&cid=' + config.cid;
		my.get(protocol + config.gamificationUrl + paramObj.action + paramsData, null, my.parseLogin);
	};


	my.checkLogin = function (obj) {
		if (typeof(obj.uid) == 'undefined') {
			if (monitorClose) {
				if (typeof(config.closeWindowCallback) != 'undefined') {
					config.closeWindowCallback(obj);
				}
				monitorClose = false;
			}
			my.resetToken();
		} else {
                    if (_isIos() && !isSafari) {
                            var key = my.getInitedKey();
                            _data(key, 1, null, 1);
                    }
                    currentToken = my.getToken();
                    var socialprovider = obj.socialProvider;
                    var uuid = _cookie('iUUID') == null ? '' : _cookie('iUUID');
                    var uparams = 'uuid=' + uuid + '&provider=' + socialprovider + '&provider_identifier=' + obj.providers[socialprovider.toLowerCase()].identifier
                    + '&e=' + encodeURIComponent(btoa(rot13(obj.mail))) + '&cid=' + config.cid + '&token=' + my.getToken() + '&euid=' + obj.uid;

                    if (!_cookie('ep_up') || _cookie('ep_up') == "null") {
                        _cookie('ep_up', socialprovider + '_' + obj.providers[socialprovider.toLowerCase()].identifier + '_' + my.getToken(), 1);
                        _firePixel(uparams);
                    } else {
                        var chk = socialprovider + '_' + obj.providers[socialprovider.toLowerCase()].identifier + '_' + my.getToken() == _cookie('ep_up');
                        if (!chk) {
                            _cookie('ep_up', socialprovider + '_' + obj.providers[socialprovider.toLowerCase()].identifier + '_' + my.getToken(), 1);
                            _firePixel(uparams);
                        }
                    }
		}
		_checkSession();
		if (typeof(config.loginCallback) != 'undefined') {
			config.loginCallback(obj);
		}
	};

	/* Configuration */
	my.login = function (name, configuration){
		instanceName = name;
		config = {
			providers : ['Facebook', 'Twitter', 'Google'],
			sessionEndCallback: function () {alert("Your session has expired");},
			closeWindowCallback: function(obj) {},
			loginCallback : function (obj) {},
			logoutCallback : function () {},
			sessionTime: 3600,
			callbackUrl : '',
			cid : 0,
			ver : '',
			noPopup : false,
			actionParam : '',
			gamificationUrl : EngagePlusConfig.config().gamificationUrl
		};
		config = _mergeOptions(config, configuration);
		if (_isIos() && !isSafari) {
			config.noPopup = true;
			if (typeof(config.callbackUrl) == 'undefined' || config.callbackUrl == '') {
				config.callbackUrl = window.location.href;
			}
		}
		my.retrieveToken();
	};

	/* End configuration */

	my.loginAction = function (provider) {
		if(!_cookieAvailable()){
			_cookieAlert();
			return false;
		}
		var origin = window.location.protocol + '//' + window.location.hostname;
		origin = encodeURIComponent(origin);
		if (typeof(config.callbackUrl) != 'undefined' && config.callbackUrl != '') {
			var callbackUrl = config.callbackUrl;
			callbackUrl = '&callback_url=' + encodeURIComponent(callbackUrl);
		} else {
			var callbackUrl = '';
		}
		if (typeof(config.actionParam) != 'undefined') {
			var actionParam = '&actionParam=' + encodeURI(config.actionParam);
		} else {
			var actionParam = '';
		}
		var loginUrl = protocol + config.gamificationUrl + 'user/auth?provider=' +
								provider.toLowerCase()
								+ '&origin=' + origin
								+ '&t=' + my.getToken()
								+ '&cid=' + config.cid + callbackUrl	+ actionParam;
		if (config.noPopup) {
			window.location.href = loginUrl;
		} else {
			popupWindow = window.open(loginUrl, 'hybridauth',
					'location=no,menubar=no,resizable=yes,scrollbars=yes,' +
					'status=no,titlebar=yes,toolbar=no,channelmode=yes,fullscreen=yes,' +
					'width=800,height=500');
			popupWindow.focus();
			my.startMonitorPopupWindow();
		}
	};

	my.init = function () {
		inited = true;
		my.setToken();
		synchronousConfig.push({
			action : "user"
		});
		my.synchronousAction();
	};

	my.send = function (url, callback, method, data, sync) {
		var x = my.x();
		if (typeof sync === 'undefined') {
			sync = false;
		}
		x.open(method, url, sync);
		x.onreadystatechange = function () {
			if (x.readyState === 4) {
				callback(x.responseText);
			}
		};
		if (method === 'POST') {
			x.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
		}
		x.send(data);
	};

	my.setCallbackURL = function (callbackUrl) {
		config.callbackUrl = callbackUrl;
	};

	my.synchronousAction = function () {
		if (synchronousConfig.length > 0) {
			var curConfig = synchronousConfig[0];
			synchronousConfig.splice(0, 1);
			curConfig.synchronousCallback = '1';
			my.action(curConfig);
		}
	};

	my.retrieveData = function(callbackName) {
		if (typeof(callbackName) == 'undefined') {
			var callbackName = '';
		}
		my.action({
			action: "user",
			callback: callbackName
		});
	};

	my.x = function () {
		if (typeof XMLHttpRequest !== 'undefined') {
			return new XMLHttpRequest();
		}
		var versions = [
			'MSXML2.XmlHttp.5.0',
			'MSXML2.XmlHttp.4.0',
			'MSXML2.XmlHttp.3.0',
			'MSXML2.XmlHttp.2.0',
			'Microsoft.XmlHttp'
		];
		var xhr;
		for (var i = 0; i < versions.length; i++) {
			try {
				xhr = new ActiveXObject(versions[i]);
				break;
			} catch (e) {}
		}
		return xhr;
	};

	my.parseLogin = function (data) {
		userData = JSON.parse(data);
		eval(instanceName).checkLogin(userData);
	};

	my.get = function (url, data, callback, sync) {
		var query = [];
		for (var key in data) {
			query.push(encodeURIComponent(key) + '=' + encodeURIComponent(data[key]));
		}
		if (url.indexOf("?") == -1) {
			url = url + '?' + query.join('&');
		} else {
			url = url + '&' + query.join('&')
		}
		my.send(url, callback, 'GET', null, sync);
	};

	my.monitorPopupWindow = function() {
		if (popupWindow.closed || !popupWindow.window) {
			my.stopMonitorPopupWindow();
			my.action({
					action: "user",
					callback: instanceName + ".checkLogin"
			});
		}
	};

	my.stopMonitorPopupWindow = function() {
		clearInterval(monitorInterval);
	};

	my.startMonitorPopupWindow = function() {
		monitorClose = true;
		monitorInterval = setInterval(instanceName + '.monitorPopupWindow()', 1000);
	};

	my.serialize = function (paramObj) {
		var str = [];
		for (var p in paramObj)
			str.push(encodeURIComponent(p) + "=" + encodeURIComponent(paramObj[p]));
		return str.join("&");
	};

	my.setToken = function () {
		if (!my.getToken() || my.getToken() == "null") {
			_data('engageplus_t' + config.cid, token + config.cid, null, 1);
		}
	};

	my.resetToken = function () {
		_data('engageplus_t' + config.cid, token + config.cid, null, 1);
		_data(my.getInitedKey(), null, null, 1);
	};

	my.logout = function () {
		var logoutUrl = protocol + config.gamificationUrl + 'user/logout?token=' + my.getToken() + '&cid=' + config.cid;
		my.get(logoutUrl, null, config.logoutCallback);
		userData = {};
		currentToken = "";
		if (_isIos() && !isSafari) {
			_data(my.getInitedKey(), null, null, 1);
		}
		my.resetToken();
		if (inited == true) {
			my.checkLogin({});
		}
	};

	my.getData = function () {
		return userData;
	};

	my.getToken = function () {
		return _data('engageplus_t' + config.cid, undefined, undefined, 1);
	};

	my.retrieveToken = function () {
		token = my.guid();
		my.setToken();
	};

	my.s4 = function () {
		return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
	};

	my.guid = function () {
		return my.s4() + my.s4() + my.s4() + my.s4() +
		my.s4() + my.s4() + my.s4() + my.s4();
	};

	my.getInitedKey = function () {
		return window.location.host.substring(0, window.location.host.indexOf('.')) + '_inited' + config.cid;
	};

	setTimeout(function() {
		if(!_cookieAvailable()){
			_cookieAlert();
		}
	}, 2000);

	return my;
}());