var EngagePlusConfig = (function () {
  var my = {},
    settings = {
      gamificationUrl : 'contenthub.innity-asia.com/2019/id/uphplus/api/'
    };
	function _mergeOptions(oldData, newData) {
		var latestData = {};
		if (typeof oldData !== 'undefined') {
			for (var attrname in oldData) {
				latestData[attrname] = oldData[attrname];
			}
		}
		if (typeof newData !== 'undefined') {
			for (var attrname in newData) {
				latestData[attrname] = newData[attrname];
			}
		}
		return latestData;
	}
  my.config = function (newSettings) {
    settings = _mergeOptions(settings, newSettings);
    return settings;
  };
  return my;
}());
