var EngagePlusWidget = (function () {
	var my = {},
		config = [],
		instanceName,
		campaignId = 0,
		api,
		widgetLoaded = false,
		animation = false,
		tabReload = true,
		loggedIn = false,
		oldCookie = null,
		pointWord = ['score', 'scores'],
		splashShow = false,
		splashCount = 0,
		timeoutAlertAt = 0,
		timeoutAlertInterval = 60,
		userRank = -1,
		userMonthlyRank = -1,
		achievementCount = 0,
		baseUrl = window.location.href.substring(0, window.location.href.lastIndexOf('/')+1),
		lastActive = 0,
		scriptCount = 0,
		actionList = [],
		pendngDel = [],
		template = [],
		animation = _animationAvailable();
	function _animationAvailable() {
		var animationStr = 'animation',
		domPrefixes = 'Webkit Moz O ms Khtml'.split(' '),
		framePrefix = '',
		element = document.createElement('div'),
		prefix = '';
		if (element.style.animationName !== 'undefined') {
			return true;
		}
		for (var i = 0; i < domPrefixes.length; i++) {
			if (element.style[domPrefixes[i] + 'AnimationName'] !== 'undefined') {
				prefix = domPrefixes[i];
				animationStr = prefix + 'Animation';
				framePrefix = '-' + prefix.toLowerCase() + '-';
				return true;
			}
		}
		return false;
	}

	/* Set / Get data */
	my.data = function (name, configuration){
		instanceName = name;
		config = {
			showLoginPanel : true,
			showSplashTab : false,
			showUserPanel : false,
			loginAlertCallback : function () {},
			loginCallback : function (obj) {},
			logoutCallback : function () {},
			initCallback : function () {},
			allActivityCallback : function (data) {},
			myActivityCallback : function (data) {},
			cashMultiplier : 1,
			showCash : false,
			providers : ['Facebook'],
			campaignId : '', // required
			splashHtml : '', // custom html for splash tab
			ver : 0.2,
			theme : 0.2,
			uid : 0
		};
		config = _mergeOptions(config, configuration);
		campaignId = config.campaignId;
		if (typeof EngagePlusLogin === 'undefined') {
			EngagePlusLogin = {};
		}
		if (config.api) {
			api = config.api;
		} else {
			EngagePlusAPI.data('Gamification', config);
			api = EngagePlusAPI;
		}
		if (config.login) {
			EngagePlusLogin[campaignId] = config.login;
		} else {
			EngagePlus.login('EngagePlusLogin[' + campaignId + ']', {
				cid : campaignId,
				'providers' : config.providers,
				loginCallback : my.loginCallback,
				containerId : loginElId,
				noPopup : true,
				callbackUrl : window.location.href
			});
			EngagePlusLogin[campaignId] = EngagePlus;
		}
		config.loginInstance = EngagePlusLogin[campaignId];
		oldCookie = EngagePlusLogin[campaignId].getToken();

		my.onloadFunc();
		EngagePlusLogin[campaignId].init();
	}
	/* End Set / Get data */

	function _mergeOptions(oldData, newData) {
		var latestData = {};
		if (typeof oldData !== 'undefined') {
			for (var attrname in oldData) {
				latestData[attrname] = oldData[attrname];
			}
		}
		if (typeof newData !== 'undefined') {
			for (var attrname in newData) {
				latestData[attrname] = newData[attrname];
			}
		}
		return latestData;
	}

	my.getTemplate = function (name, preload) {
		if (template[name]) {
			return template[name];
		}
		api.get(baseUrl + 'gamification/template/' + name + '.html?2016', null, function (data) {
			template[name] = data;
			return template[name];
		}, preload);
		return template[name];
	};

	my.userLogin = function (obj) {
		config.uid = obj.uid;
		my.loginCallback(obj);
	};

	 my.getAPI = function() {
		return api;
	};

	my.loginCallback = function (userObj) {
		userObj.apiToken = EngagePlusLogin[campaignId].getToken();
		api.user(userObj);
		var preload = true;
		if (userObj.uid > 0) {
			if (loggedIn) {
				return false;
			}
			api.user({
				currentRank : api.userRank()
			});
			loggedIn = true;
			config.uid = userObj.uid;
		} else {
			if (config.showSplashTab) {
				api.user({
					'name' : 'Guest',
					uid : 0
				}, true);
			}
			config.initCallback(userObj);
		} //end if
	};

	my.loginAlert = function () {
		config.loginAlertCallback();
	};

	my.onloadFunc = function () {
		if(widgetLoaded) {
			return false;
		}
		widgetLoaded = true;
		window['EngagePlusGamificationGlobal'] = my;
	};

	my.loginAction = function (provider) {
		config.loginInstance.loginAction(provider);
	};

        my.redeemProduct = function (productId) {
            api.redeem(productId, function (data) {
                if (data.errorDesc) {
                    alert(data.errorDesc);
                } else if (data.message) {
                    alert(data.message);
                    if (data.totalCoins) {
                        api.user({'coins': data.totalCoins});
                    }
                }
            });
        };

	my.logout = function () {
		loggedIn = false;
		var reset = true;
		api.user(undefined, reset);
		api.closeFanout();
		config.uid = 0;
		config.loginInstance.logout();
		config.logoutCallback();
		if (!config.showSplashTab) {
			setTimeout(function () {
				window.location.reload(false)
			}, 1000);
		}
	};

	my.loadScript = function (scriptId, scriptSrc) {
		var index = scriptSrc.lastIndexOf('.') + 1,
		isCss = scriptSrc.substring(index, index + 3).toLowerCase() === 'css',
		protocol = scriptSrc.substring(0, scriptSrc.indexOf('://')).toLowerCase(),
		locally = protocol === '';
		if ( document.getElementById(scriptId) === null) {
			if (isCss) {
				var g = document.createElement('link');
			} else {
				var g = document.createElement('script');
			}
			var s = document.getElementsByTagName('script')[0];
			if (isCss) {
				g.setAttribute('rel', 'stylesheet');
				g.setAttribute('type', 'text/css');
				if (locally) {
					g.setAttribute('href', baseUrl + scriptSrc);
				} else {
					g.setAttribute('href', scriptSrc);
				}
			} else {
				if (locally) {
					g.src = baseUrl + scriptSrc;
				} else {
					g.src = scriptSrc;
				}
			}
			g.async = true;
			g.setAttribute('id', scriptId);
			s.parentNode.insertBefore(g, s);
		}
	};

	return my;
}());