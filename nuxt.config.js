export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Laneige Water Sleeping Mask EX | All New Formula',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Beyond hydration, wake up to vibrant skin with probiotic complex. Sign-up and redeem your complimentary Better Sleep sampling kit now!' },
      { property: 'og:title', content:  'Laneige Water Sleeping Mask EX | All New Formula' },
      { property: 'og:description', content: 'Beyond hydration, wake up to vibrant skin with probiotic complex. Sign-up and redeem your complimentary Better Sleep sampling kit now!' },
      { property: 'og:image', content: process.env.BASE_URL + 'dist/img/meta.jpg' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: process.env.BASE_URL + 'dist/lanegefavicon114x114.ico' },
      { rel: 'icopreconnectn', href: 'https://fonts.gstatic.com' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css2?family=Montserrat:wght@400;700&display=swap' }
    ],
    script: [
      { src: "js/jquery-2.1.1.js", type: "text/javascript", body: true },
      { src: "js/bootstrap.min.js", type: "text/javascript", body: true },
      { src: "js/perfect-scrollbar.min.js", type: "text/javascript", body: true },
      { src: "js/slick.min.js", type: "text/javascript", body: true },
      { src: "js/slick-animation.js", type: "text/javascript", body: true },
      { src: "js/gsap/gsap-3.0.1.min.js", type: "text/javascript", body: true },
      { src: "js/gsap/ScrollMagic.js", type: "text/javascript", body: true },
      { src: "js/jquery.dotdotdot.min.js", type: "text/javascript", body: true },
      { src: "js/infinite-scroll.pkgd.min.js", type: "text/javascript", body: true },
      { src: "js/custom.js", type: "text/javascript", body: true },
      { src: "js/footer_script.js", type: "text/javascript", body: true },
      { src: "js/sh/badwords.js", type: "text/javascript", body: true },
      { src: "js/sh/comment.js", type: "text/javascript", body: true },
      { src: "js/sh/script.js", type: "text/javascript", body: true },
      { src: "js/redeem.js", type: "text/javascript", body: true },
      { src: "gamification/js/2.0/config.js", type: "text/javascript", body: true },
      { src: "gamification/js/2.0/login.js", type: "text/javascript", body: true },
      { src: "gamification/js/2.0/api.js", type: "text/javascript", body: true },
      { src: "gamification/js/2.0/widget.js", type: "text/javascript", body: true },
      { src: "gamification/js/loader.js", type: "text/javascript", body: true },
      { src: "gamification/js/callback.js", type: "text/javascript", body: true }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '~assets/css/main.css',
    '~assets/css/perfect-scrollbar.css',
    '~assets/css/slick.css',
    '~assets/css/slick-theme.css',
    '~assets/css/animate.min.css',
    '~assets/css/custom.css',
    '~assets/css/game.css'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: '~/plugins/slick'}
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/bootstrap
    'bootstrap-vue/nuxt',
    // apply axios to every component
    '@nuxtjs/axios'
  ],

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: { 
  },

  // public folder
  router: {
    base: process.env.BASE_PATH
  },

/*  axios: {
    baseURL: 'http://localhost/laneige-demo/api/'
  },*/

  // public env
  publicRuntimeConfig: {
    baseURL: process.env.BASE_URL
  },

  // private env
  privateRuntimeConfig: {
    apiSecret: process.env.API_SECRET
  }

}
