import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from 'ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _57a49317 = () => interopDefault(import('..\\pages\\game.vue' /* webpackChunkName: "pages/game" */))
const _f15226fe = () => interopDefault(import('..\\pages\\redeem.vue' /* webpackChunkName: "pages/redeem" */))
const _a9ba0c5c = () => interopDefault(import('..\\pages\\social.vue' /* webpackChunkName: "pages/social" */))
const _60457488 = () => interopDefault(import('..\\pages\\social\\post\\_slug.vue' /* webpackChunkName: "pages/social/post/_slug" */))
const _33bff67d = () => interopDefault(import('..\\pages\\index.vue' /* webpackChunkName: "pages/index" */))

const emptyFn = () => {}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/laneige-demo/dist/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/game",
    component: _57a49317,
    name: "game"
  }, {
    path: "/redeem",
    component: _f15226fe,
    name: "redeem"
  }, {
    path: "/social",
    component: _a9ba0c5c,
    name: "social",
    children: [{
      path: "post/:slug?",
      component: _60457488,
      name: "social-post-slug"
    }]
  }, {
    path: "/",
    component: _33bff67d,
    name: "index"
  }],

  fallback: false
}

export function createRouter (ssrContext, config) {
  const base = (config._app && config._app.basePath) || routerOptions.base
  const router = new Router({ ...routerOptions, base  })

  // TODO: remove in Nuxt 3
  const originalPush = router.push
  router.push = function push (location, onComplete = emptyFn, onAbort) {
    return originalPush.call(this, location, onComplete, onAbort)
  }

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    return resolve(to, current, append)
  }

  return router
}
