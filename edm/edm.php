<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable"
	style="font-family:Helvetica; max-width: 600px; margin: auto;">
	<tr>
		<td align="center" valign="top" id="bodyCell">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td align="center" valign="top" id="templatePreheader">
						<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%"
							class="templateContainer">
							<tr>
								<td valign="top" class="preheaderContainer"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="center" valign="top" id="templateHeader">
						<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%"
							class="templateContainer">
							<tr>
								<td valign="top" class="headerContainer">
									<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageBlock"
										style="min-width:100%;">
										<tbody class="mcnImageBlockOuter">
											<tr>
												<td valign="top" style="padding:9px" class="mcnImageBlockInner">
													<table align="left" width="100%" border="0" cellpadding="0"
														cellspacing="0" class="mcnImageContentContainer"
														style="min-width:100%;">
														<tbody>
															<tr>
																<td class="mcnImageContent" valign="top"
																	style="padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0; text-align:center;">


																	<img align="center" alt="" src="https://contenthub.innity-asia.com/2021/sg/laneige/staging/edm/img/img1.png"
																		width="564"
																		style="max-width:1210px; padding-bottom: 0; display: inline !important; vertical-align: bottom;"
																		class="mcnImage">


																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="center" valign="top" id="templateBody">
						<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%"
							class="templateContainer">
							<tr>
								<td valign="top" class="bodyContainer">
									<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock"
										style="min-width:100%;">
										<tbody class="mcnTextBlockOuter">
											<tr>
												<td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
													<table align="left" border="0" cellpadding="0" cellspacing="0"
														style="max-width:100%; min-width:100%;" width="100%"
														class="mcnTextContentContainer">
														<tbody>
															<tr>

																<td valign="top" class="mcnTextContent"
																	style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">

																	<div style="text-align: center;">
																		<p
																			style="font-size: 28px; font-weight: bold; text-align: center;">
																			You have gained <span
																				style="color:#0070C0"><?php echo $score; ?>
																				points</span><br>
																			and you are entitled to a <span
																				style="color:#0070C0"><?php echo $discount; ?>%
																				discount</span></p>

																		<p style="font-size: 18px; text-align: center;">
																			on the New Water Sleeping Mask EX as well as
																			a deluxe gift <?php if ($score >= 100): ?>
																				Redeemable at LANEIGE boutiques and department store counters
																			<?php else: ?>
																				only redeemable at LANEIGE boutiques
																			<?php endif;?></p>
																	</div>

																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
										</tbody>
									</table>
									<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock"
										style="min-width:100%;">
										<tbody class="mcnTextBlockOuter">
											<tr>
												<td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
													<table align="left" border="0" cellpadding="0" cellspacing="0"
														style="max-width:100%; min-width:100%;" width="100%"
														class="mcnTextContentContainer">
														<tbody>
															<tr>
																<td valign="top" class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">
																	<div style="text-align: center;">1. Claim your discount voucher through our <strong style="color:#0070C0; font-weight:bold">Lazada store</strong>
																	</div>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
										</tbody>
									</table>
									<table border="0" cellpadding="0" cellspacing="0" width="100%"
										class="mcnButtonBlock" style="min-width:100%;">
										<tbody class="mcnButtonBlockOuter">
											<tr>
												<td style="padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;"
													valign="top" align="center" class="mcnButtonBlockInner">
													<table border="0" cellpadding="0" cellspacing="0"
														class="mcnButtonContentContainer"
														style="border-collapse: separate !important;border-radius: 7px;background-color: #0070C0;">
														<tbody>
															<tr>
																<td align="center" valign="middle"
																	class="mcnButtonContent"
																	style="font-family: Arial; font-size: 16px; padding: 15px 25px;border-radius:10px;">
																	<a class="mcnButton " title="Claim Now" href="<?php echo $discount_link; ?>"
																		target="_blank"
																		style="font-weight: bold;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;">Claim
																		Now</a>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
										</tbody>
									</table>
									<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock"
										style="min-width:100%;">
										<tbody class="mcnTextBlockOuter">
											<tr>
												<td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
													<table align="left" border="0" cellpadding="0" cellspacing="0"
														style="max-width:100%; min-width:100%;" width="100%"
														class="mcnTextContentContainer">
														<tbody>
															<tr>

																<td valign="top" class="mcnTextContent"
																	style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">

																	<div style="text-align: center;">2. Present this
																		email to our beauty advisors at any <strong
																			color:=""
																			style="color:#0070C0; font-weight:bold">LANEIGE</strong><br><strong
																			color:=""
																			style="color:#0070C0; font-weight:bold">boutiques
																			to redeem your deluxe-size gift.</strong>
																	</div>

																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
										</tbody>
									</table>
									<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock"
										style="min-width:100%;">
										<tbody class="mcnTextBlockOuter">
											<tr>
												<td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
													<table align="left" border="0" cellpadding="0" cellspacing="0"
														style="max-width:100%; min-width:100%;" width="100%"
														class="mcnTextContentContainer">
														<tbody>
															<tr>

																<td valign="top" class="mcnTextContent"
																	style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">

																	<div style="text-align: center;">3. You can also use
																		the <span color:=""
																			style="color:#0070C0;font-weight:bold;">discount
																			code below for in-store redemption.</span>
																	</div>

																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
										</tbody>
									</table>
									<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock"
										style="min-width:100%;">
										<tbody class="mcnTextBlockOuter">
											<tr>
												<td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
													<table align="left" border="0" cellpadding="0" cellspacing="0"
														style="max-width:100%; min-width:100%;" width="100%"
														class="mcnTextContentContainer">
														<tbody>
															<tr>

																<td valign="top" class="mcnTextContent"
																	style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">

																	<div style="font-size:28px;text-align: center;"><?php echo $discount; ?>%
																		off: <strong color:=""
																			style="color:#0070C0; font-weight:bold"><?php echo $voucher_code; ?></strong>
																	</div>

																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="center" valign="top" id="templateFooter">
						<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%"
							class="templateContainer">
							<tr>
								<td valign="top" class="footerContainer">
									<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock"
										style="min-width:100%;">
										<tbody class="mcnTextBlockOuter">
											<tr>
												<td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
													<table align="left" border="0" cellpadding="0" cellspacing="0"
														style="max-width:100%; min-width:100%;" width="100%"
														class="mcnTextContentContainer">
														<tbody>
															<tr>
																<td valign="top" class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">
																	<div style="text-align: center;">This promotion is only valid till <span color:="" style="color:#0070C0;font-weight:bold;">18 April 2021.</span>
																	</div>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>