<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable"
	style="font-family:Helvetica; max-width: 600px; margin: auto;">
	<tr>
		<td align="center" valign="top" id="bodyCell">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td align="center" valign="top" id="templatePreheader">
						<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%"
							class="templateContainer">
							<tr>
								<td valign="top" class="preheaderContainer"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="center" valign="top" id="templateHeader">
						<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%"
							class="templateContainer">
							<tr>
								<td valign="top" class="headerContainer">
									<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageBlock"
										style="min-width:100%;">
										<tbody class="mcnImageBlockOuter">
											<tr>
												<td valign="top" style="padding:9px" class="mcnImageBlockInner">
													<table align="left" width="100%" border="0" cellpadding="0"
														cellspacing="0" class="mcnImageContentContainer"
														style="min-width:100%;">
														<tbody>
															<tr>
																<td class="mcnImageContent" valign="top"
																	style="padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0; text-align:center;">


																	<img align="center" alt="" src="https://contenthub.innity-asia.com/2021/sg/laneige/staging/edm/img/img1.png"
																		width="564"
																		style="max-width:1210px; padding-bottom: 0; display: inline !important; vertical-align: bottom;"
																		class="mcnImage">


																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="center" valign="top" id="templateBody">
						<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%"
							class="templateContainer">
							<tr>
								<td valign="top" class="bodyContainer">
									<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock"
										style="min-width:100%;">
										<tbody class="mcnTextBlockOuter">
											<tr>
												<td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
													<table align="left" border="0" cellpadding="0" cellspacing="0"
														style="max-width:100%; min-width:100%;" width="100%"
														class="mcnTextContentContainer">
														<tbody>
															<tr>
																<td valign="top" class="mcnTextContent"
																	style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">
																	<div style="text-align: center;">Wind down, relax and fall asleep with our iconic Water Sleeping Mask as
																		well as Cica Sleeping Mask. Wake-up to bright and clear complexion.To redeem your complimentary sampling kit, present this email to our beauty advisors on
																	</div>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
										</tbody>
									</table>
									<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock"
										style="min-width:100%;">
										<tbody class="mcnTextBlockOuter">
											<tr>
												<td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
													<table align="left" border="0" cellpadding="0" cellspacing="0"
														style="max-width:100%; min-width:100%;" width="100%"
														class="mcnTextContentContainer">
														<tbody>
															<tr>
																<td valign="top" class="mcnTextContent"
																	style="font-size: 28px; font-weight: bold; padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">
																	<div style="text-align: center;">
																		<strong color:="" style="color:#0070C0; font-weight:bold"><?php echo $date; ?></strong>
																		at
																		<strong color:="" style="color:#0070C0; font-weight:bold"><?php echo $store; ?></strong>
																	</div>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
										</tbody>
									</table>
									<table border="0" cellpadding="0" cellspacing="0" width="100%"
										class="mcnButtonBlock" style="min-width:100%;">
										<tbody class="mcnButtonBlockOuter">
											<tr>
												<td style="padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;"
													valign="top" align="center" class="mcnButtonBlockInner">
													<table border="0" cellpadding="0" cellspacing="0"
														class="mcnButtonContentContainer"
														style="border-collapse: separate !important;border-radius: 7px;background-color: #0070C0;">
														<tbody>
															<tr>
																<td align="center" valign="middle"
																	class="mcnButtonContent"
																	style="font-family: Arial; font-size: 16px; padding: 15px 25px;border-radius:10px;">
																	<a class="mcnButton " title="Redeem Now" href="<?php echo $link; ?>"
																		target="_blank"
																		style="font-weight: bold;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;">Redeem
																		Now</a>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="center" valign="top" id="templateFooter">
						<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%"
							class="templateContainer">
							<tr>
								<td valign="top" class="footerContainer">
									<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock"
										style="min-width:100%;">
										<tbody class="mcnTextBlockOuter">
											<tr>
												<td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
													<table align="left" border="0" cellpadding="0" cellspacing="0"
														style="max-width:100%; min-width:100%;" width="100%"
														class="mcnTextContentContainer">
														<tbody>
															<tr>

																<td valign="top" class="mcnTextContent"
																	style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">

																	<div
																		style="font-size: 20px; font-weight: bold; text-align: left;padding-bottom:10px;">
																		<span style="color:#000000">Terms and
																			Conditions:</span></div>

																	<ol style="padding:0px 20px;">
																		<li
																			style="text-align: left;padding-bottom:10px;line-height:1.5;">
																			<span style="color:#252525"><span
																					style="font-size:15px">Limited to one entry & redemption per customer.</span></span></li>
																		<li
																			style="text-align: left;padding-bottom:10px;line-height:1.5;">
																			<span style="color:#252525"><span
																					style="font-size:15px">Sample kit redemption can only be collected at the store selected
																				during the indicated date.</span></span></li>
																		<li
																			style="text-align: left;padding-bottom:10px;line-height:1.5;">
																			<span style="color:#252525"><span
																					style="font-size:15px">Your contact information may be used to contact you regarding your sample kit redemption,
																				as such we are not responsible for errors due to incorrect input information.</span></span></li>
																		<li
																			style="text-align: left;padding-bottom:10px;line-height:1.5;">
																			<span style="color:#252525"><span
																					style="font-size:15px">In the event that sample runs out due to overwhelming response, you will be put on waiting list.</span></span>
																		</li>
																		<li
																			style="text-align: left;padding-bottom:10px;line-height:1.5;">
																			<span style="color:#252525"><span
																					style="font-size:15px">The management reserves the right to amend the offer without prior notice.</span></span></li>
																		<li
																			style="text-align: left;padding-bottom:10px;line-height:1.5;">
																			<span style="color:#252525"><span
																					style="font-size:15px">Other terms and conditions apply.</span></span>
																		</li>
																	</ol>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>