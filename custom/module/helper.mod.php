<?php
// Function to check unique data
function verifyDuplicate($table, $param, $dbparam) {
    global $dbhandler;
    $sql = $dbhandler->prepare("SELECT * FROM " . $table . " WHERE $param = :dbparam");
    $sql->bindParam(':dbparam', $dbparam);
    $sql->execute();
    $res = $sql->fetch(PDO::FETCH_ASSOC);
    return $res;
}

// Check redeem email and redeem_code is valid
function verifyRedeemCode($table, $email, $code) {
    global $dbhandler;
    $sql = $dbhandler->prepare("SELECT * FROM " . $table . " WHERE email = :email AND redeem_code = :code");
    $sql->bindParam(':email', $email);
    $sql->bindParam(':code', $code);
    $sql->execute();
    $res = $sql->fetch(PDO::FETCH_ASSOC);
    return $res;
}

// Generate code
function generateRandomString($digit, $alphabet, $symbol, $custom, $no_digit, $no_alphabet, $no_symbol, $no_custom) {

    // Initialize
    $string = '';

    // If there's digit
    if ( $no_digit ){
        for ($x = 0; $x < $no_digit; $x++){
            $string .= $digit[rand(0, strlen($digit) - 1)];
        }
    }
    // If there's alphabet
    if ( $no_alphabet ){
        for ($x = 0; $x < $no_alphabet; $x++){
            $string .= $alphabet[rand(0, strlen($alphabet) - 1)];
        }
    }
    // If there's symbol
    if ( $no_symbol ){
        for ($x = 0; $x < $no_symbol; $x++){
            $string .= $symbol[rand(0, strlen($symbol) - 1)];
        }
    }
    // If there's custom
    if ( $no_custom ){
        for ($x = 0; $x < $no_custom; $x++){
            $string .= $custom[rand(0, strlen($custom) - 1)];
        }
    }

    // Shuffle String
    $randomString = str_shuffle($string);

    // Return String
    return $randomString;
}