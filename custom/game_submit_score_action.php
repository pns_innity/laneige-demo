<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST');

// error_reporting(E_ALL);
// ini_set("display_errors", 1);
require_once "global.php";
require_once "config.inc.php";
include "module/email.php";
include "module/helper.mod.php";
date_default_timezone_set("Asia/Kuala_lumpur");

if (!$_POST) {
    echo '{"status" : 0, "msg" : "Please try again."}';
    exit();
}

if (!empty($_POST['email'])) {
	$email = trim($_POST['email']);
} else {
	echo '{"status" : 0, "msg" : "Email field can not be blank."}';
	exit();
}

// for int value
if (isset($_POST['score'])) {
	$score = $_POST['score'];
} else {
	echo '{"status" : 0, "msg" : "Score field can not be blank."}';
	exit();
}


$hostname = $_SERVER['REMOTE_ADDR'];
$browser  = $_SERVER['HTTP_USER_AGENT'];
$created = date(DB_DATETIME_FORMAT);

// check if user already register
$verifyEmail = verifyDuplicate($table["user"], 'email', $email);
// check exists user on entries
$userEntries = verifyDuplicate($table["entries"], 'email', $email);


// tier 1 (20-99 point): 10%
// tier 2 (>100 points): 15%
if ($score >= 20 && $score <= 99) {
	// tier 1
	$voucher_code = 'WSMEX10OFF';
	$discount = '10';
	$discount_link = 'https://www.lazada.sg/shop/i/landing_page/voucher?sellerId=15453&voucherId=10580077715453&wh_weex=true&spm=a2o42.innity.microsite.10';
} elseif ($score >= 100) {
	// tier 2
	$voucher_code = 'WSMEX15OFF';
	$discount = '15';
	$discount_link = 'https://www.lazada.sg/shop/i/landing_page/voucher?sellerId=15453&voucherId=10580270915453&wh_weex=true&spm=a2o42.innity.microsite.15';
} else {
	$voucher_code = '';
}

// check valid user before update
if ($verifyEmail && $userEntries) {

	if ($verifyEmail['score'] === null) {
		// update user final score
		try{
			$stmt = $dbhandler->prepare("UPDATE " . $table["user"] . " SET score = :score, voucher_code = :voucher_code WHERE email = :email ");
			$stmt->bindParam(':email', $email);
			$stmt->bindParam(':score', $score);
			$stmt->bindParam(':voucher_code', $voucher_code);
			$upt = $stmt->execute();
		} catch (Exception $ex) {
			echo '{"status":"0"}';
			exit();
		}

		if ($upt) {
			// send email if score > 20
			if ($score >= 20) {
				game_email($email, $score, $discount, $discount_link, $voucher_code);
				echo '{"status":"1"}';
				exit();
			} else {
				echo '{"status" : 3, "msg":"Score below 20"}';
				exit();
			}
		} else {
			echo '{"status":"0"}';
			exit();
		}

	} else {
		echo '{"status" : 2, "msg":"Oops! It looks like you have already submitted your score. Scores can only be submitted once per participant."}';
		exit();
	}

} else {
	echo '{"status" : 0, "msg":"Email not exists"}';
	exit();
}