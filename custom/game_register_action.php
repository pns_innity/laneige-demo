<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST');

// error_reporting(E_ALL);
// ini_set("display_errors", 1);
require_once "global.php";
require_once "config.inc.php";
include "module/helper.mod.php";
date_default_timezone_set("Asia/Kuala_lumpur");

if (!$_POST) {
    echo '{"status" : 0, "msg" : "Please try again."}';
    exit();
}

if (!empty($_POST['name'])) {
	$name = trim($_POST['name']);
} else {
	echo '{"status" : 0, "msg" : "Name can not be blank."}';
	exit();
}

if (!empty($_POST['mobile'])) {
	$mobile = trim($_POST['mobile']);
} else {
	echo '{"status" : 0, "msg" : "Mobile field can not be blank."}';
	exit();
}

if (!empty($_POST['email'])) {
	$email = trim($_POST['email']);
} else {
	echo '{"status" : 0, "msg" : "Email field can not be blank."}';
	exit();
}

if ($_POST['agree'] == 'true' || $_POST['agree'] == '1') {
	$agree = '1';
} elseif ($_POST['agree'] == 'false' || $_POST['agree'] == '0') {
	$agree = '0';
} else {
	echo '{"status" : 0, "msg" : "Agree field required."}';
	exit();
}


$hostname = $_SERVER['REMOTE_ADDR'];
$browser  = $_SERVER['HTTP_USER_AGENT'];
$created = date(DB_DATETIME_FORMAT);

// check if user already register
$verifyEmail = verifyDuplicate($table["user"], 'email', $email);


if ($verifyEmail) {

	// If user register before but score is null : return status=1
	// If user registered before and score is not null :return status =2 ,
	if ($verifyEmail['score'] == null) {
		echo '{"status" : 1, "msg":"continue to play"}';
		exit();
	} elseif ($verifyEmail['score'] != null && $verifyEmail['score'] < 20){
		echo '{"status" : 3, "msg":"Score below 20"}';
		exit();
	} else {
		echo '{"status" : 2, "msg":"Oops! It looks like you have already submitted your score. Scores can only be submitted once per participant."}';
		exit();
	}

} else {
	// If user no registered : insert and return status=1
	$attribute1 = 'name, mobile, email, agree, hostname, browser, created';
	$attribute2 = ":name, :mobile, :email, :agree, :hostname, :browser, :created";

	try{
		$stmt = $dbhandler->prepare("INSERT INTO ".$table["user"]." (".$attribute1.") VALUES (".$attribute2.")");
		$stmt->bindParam(':name', $name);
		$stmt->bindParam(':mobile', $mobile);
		$stmt->bindParam(':email', $email);
		$stmt->bindParam(':agree', $agree);
		$stmt->bindParam(':hostname', $hostname);
		$stmt->bindParam(':browser', $browser);
		$stmt->bindParam(':created', $created);
		$res = $stmt->execute();
	} catch (Exception $ex) {
		echo '{"status":"0"}';
		exit();
	}

	if ($res) {
		echo '{"status":"1", "msg":"registered!!!"}';
		exit();
	} else {
		echo '{"status":"0"}';
		exit();
	}
}