<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST');

// error_reporting(E_ALL);
// ini_set("display_errors", 1);
require_once "global.php";
require_once "config.inc.php";
include "module/helper.mod.php";
date_default_timezone_set("Asia/Kuala_lumpur");

if (!$_POST) {
    echo '{"status" : 0, "msg" : "Please try again."}';
    exit();
}

if (!empty($_POST['email'])) {
	$email = trim($_POST['email']);
} else {
	echo '{"status" : 0, "msg" : "Email field can not be blank."}';
	exit();
}

// for int value
if (isset($_POST['score'])) {
	$score = $_POST['score'];
} else {
	echo '{"status" : 0, "msg" : "Score field can not be blank."}';
	exit();
}

$hostname = $_SERVER['REMOTE_ADDR'];
$browser  = $_SERVER['HTTP_USER_AGENT'];
$created = date(DB_DATETIME_FORMAT);

// check if user already register
$verifyEmail = verifyDuplicate($table["user"], 'email', $email);


if ($verifyEmail) {

	if ($verifyEmail['score'] !== null) {
		echo '{"status":"0", "msg":"Score hes been submit."}';
		exit();
	}

	// If user exist : insert and return status=1
	$attribute1 = 'email, score, hostname, browser, created';
	$attribute2 = ":email, :score, :hostname, :browser, :created";

	try{
		$stmt = $dbhandler->prepare("INSERT INTO ".$table["entries"]." (".$attribute1.") VALUES (".$attribute2.")");
		$stmt->bindParam(':email', $email);
		$stmt->bindParam(':score', $score);
		$stmt->bindParam(':hostname', $hostname);
		$stmt->bindParam(':browser', $browser);
		$stmt->bindParam(':created', $created);
		$res = $stmt->execute();
	} catch (Exception $ex) {
		echo '{"status":"0"}';
		exit();
	}

	if ($res) {
		echo '{"status":"1"}';
		exit();
	} else {
		echo '{"status":"0"}';
		exit();
	}
} else {
	echo '{"status" : 0, "msg":"Email not exists"}';
	exit();
}