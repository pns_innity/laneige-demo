<?php
//error_reporting(E_ALL);
//ini_set("display_errors", 1);

// Database setting
define('DB_DATETIME_FORMAT', 'Y-m-d H:i:s');
define('DB_DATE_FORMAT', 'Y-m-d');
define('DB_TIME_FORMAT', 'H:i:s');

$dbhost = "localhost";
$dbuser = "demo_laneige_user";
$dbpass = "WvR4JQ564B";
$db = 'demo_sg_laneige';


/*$dbhost = "localhost";
$dbuser = "root";
$dbpass = "";
$db = 'contenthub_sg_laneige_staging';*/


// Table name
$table["registrant"] = 'redeem_registrant';
$table["user"] = 'game_users';
$table["entries"] = 'game_entries';


// Connect to database
try {
    $dbhandler = new PDO("mysql:host=$dbhost;dbname=$db", $dbuser, $dbpass);
} catch (Exception $ex) {
    echo '{"status" : 0, "msg" : "Please try again."}';
    exit();
}

$dbhandler->exec("set names utf8");
$dbhandler->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
$dbhandler->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$base_url = 'http' . (isset($_SERVER['HTTPS']) ? 's' : '') . '://' . $_SERVER['HTTP_HOST'] . rtrim(dirname($_SERVER ['SCRIPT_NAME']), '\/') . '/';