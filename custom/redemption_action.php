<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST');

// error_reporting(E_ALL);
// ini_set("display_errors", 1);
require_once "global.php";
require_once "config.inc.php";
include "module/helper.mod.php";
date_default_timezone_set("Asia/Kuala_lumpur");

if (!$_POST) {
    echo '{"status" : 0, "msg" : "Please try again."}';
    exit();
}

if (!empty($_POST['email'])) {
	$email = trim($_POST['email']);
} else {
	echo '{"status" : 0, "msg" : "Email field can not be blank."}';
	exit();
}

if (!empty($_POST['code'])) {
	$redeem_code = trim($_POST['code']);
} else {
	echo '{"status": 0}';
	exit();
}


$redeem_at = date(DB_DATETIME_FORMAT);

// check email is not exists
$verifyEmail = verifyDuplicate($table["registrant"], 'email', $email);
if ($verifyEmail == null) {
	echo '{"status": 0, "msg": "email not exists please register."}';
	exit();
}

// verified user redeem code is valid
$userRedeemCode = verifyRedeemCode($table["registrant"], $email, $redeem_code);

if ($userRedeemCode && $userRedeemCode['redeem'] == 0) {

	try {
		$stmt = $dbhandler->prepare("UPDATE " . $table["registrant"] . " SET redeem = 1, redeem_at = :redeem_at WHERE email = :email AND redeem_code = :redeem_code");
		$stmt->bindParam(':email', $email);
		$stmt->bindParam(':redeem_code', $redeem_code);
		$stmt->bindParam(':redeem_at', $redeem_at);
		$upt = $stmt->execute();
	} catch (Exception $ex) {
		echo '{"status":"0", "msg":"update error"}';
		exit();
	}

	if ($upt) {
		echo '{"status":"1"}';
		exit();
	} else {
		echo '{"status":"0", "msg":"update fail"}';
		exit();
	}

} else {
	echo '{"status" : 0, "msg":"invalid code."}'; // code has been redeem or code invalid
	exit();
}