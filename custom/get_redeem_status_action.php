<?php
require_once "config.inc.php";

$data = [];

if (!empty($_GET['email'])) {
   $email = $_GET['email'];
} else {
    echo json_encode($data);
    exit();
}


// select code and status columns only
$sql = $dbhandler->prepare("SELECT redeem as code_status FROM " . $table["registrant"] . " WHERE  email = :email" );
$sql->bindParam(':email', $email);
$sql->execute();
$res = $sql->fetch(PDO::FETCH_ASSOC);

echo json_encode($res);