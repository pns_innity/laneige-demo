<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST');

// error_reporting(E_ALL);
// ini_set("display_errors", 1);
require_once "global.php";
require_once "config.inc.php";
include "module/email.php";
include "module/helper.mod.php";
date_default_timezone_set("Asia/Kuala_lumpur");

if (!$_POST) {
    echo '{"status" : 0, "msg" : "Please try again."}';
    exit();
}

if (!empty($_POST['name'])) {
	$name = trim($_POST['name']);
} else {
	echo '{"status" : 0, "msg" : "Name can not be blank."}';
	exit();
}

if (!empty($_POST['contact'])) {
	$mobile = trim($_POST['contact']);
} else {
	echo '{"status" : 0, "msg" : "Mobile field can not be blank."}';
	exit();
}

if (!empty($_POST['email'])) {
	$email = trim($_POST['email']);
} else {
	echo '{"status" : 0, "msg" : "Email field can not be blank."}';
	exit();
}

if (!empty($_POST['store'])) {
	$store = trim($_POST['store']);
} else {
	echo '{"status" : 0, "msg" : "Store field can not be blank."}';
	exit();
}

// combine preferred_month & preferred_day
if (!empty($_POST['month']) && !empty($_POST['day']) ) {
	$preferred_month	= trim($_POST['month']);
	$preferred_day		= $_POST['day'];
	$preferred_date = strtotime($preferred_day . ' ' . $preferred_month . ' 2021');
	$preferred_date = date('Y-m-d',$preferred_date);
} else {
	echo '{"status" : 0, "msg" : "Preferred date field can not be blank."}';
	exit();
}

if (!empty($_POST['prefer'])) {
	$prefer = trim($_POST['prefer']);
} else {
	echo '{"status" : 0, "msg" : "Prefer field can not be blank."}';
	exit();
}


if ($_POST['member'] == 'true' || $_POST['member'] == '1') {
	$is_member = '1';
} elseif ($_POST['member'] == 'false' || $_POST['member'] == '0') {
	$is_member = '0';
} else {
	echo '{"status" : 0, "msg" : "Member field can not be blank."}';
	exit();
}

if ($_POST['agree'] == 'true' || $_POST['agree'] == '1') {
	$agree = '1';
} elseif ($_POST['agree'] == 'false' || $_POST['agree'] == '0') {
	$agree = '0';
} else {
	echo '{"status" : 0, "msg" : "Please check TnC field."}';
	exit();
}


$url = !empty($_POST['url']) ? trim($_POST['url']) : '';
$hostname = $_SERVER['REMOTE_ADDR'];
$browser  = $_SERVER['HTTP_USER_AGENT'];
$created = date(DB_DATETIME_FORMAT);

/*generate new code for reset password*/
//$generate_code = rand(100000000, 999999999);
// Settings
$digit = '0123456789';
$alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
$symbol = '!@#$*(';
$custom = '';

$no_digit = 4;
$no_alphabet = 2;
$no_symbol = 0;
$no_custom = 0;

$redeem_code = generateRandomString($digit, $alphabet, $symbol, $custom, $no_digit, $no_alphabet, $no_symbol, $no_custom);

// check if user already register
$verifyEmail = verifyDuplicate($table["registrant"], 'email', $email);
$verifyMobile = verifyDuplicate($table["registrant"], 'mobile', $mobile);

$base_url = str_replace('custom/', '', $base_url);
$redeem_link = $base_url . 'dist/redeem?email=' . $email . '&code=' . $redeem_code . '';


if ($verifyEmail || $verifyMobile) {

	$items = $verifyEmail ? 'email' : ($verifyMobile ? 'mobile' : '');
	$status_code = $verifyEmail ? '2' : ($verifyMobile ? '3' : '');

	echo '{"status" : ' . $status_code . ', "msg" : "Your ' . $items . ' has been use."}';
    exit();

} else {
	$attribute1 = 'name, mobile, email, store, preferred_date, prefer, is_member, agree, url, redeem_code, hostname, browser, created';
	$attribute2 = ":name, :mobile, :email, :store, :preferred_date, :prefer, :is_member, :agree, :url, :redeem_code, :hostname, :browser, :created";

	try{
		$stmt = $dbhandler->prepare("INSERT INTO ".$table["registrant"]." (".$attribute1.") VALUES (".$attribute2.")");
		$stmt->bindParam(':name', $name);
		$stmt->bindParam(':mobile', $mobile);
		$stmt->bindParam(':email', $email);
		$stmt->bindParam(':store', $store);
		$stmt->bindParam(':preferred_date', $preferred_date);
		$stmt->bindParam(':prefer', $prefer);
		$stmt->bindParam(':is_member', $is_member);
		$stmt->bindParam(':agree', $agree);
		$stmt->bindParam(':url', $url);
		$stmt->bindParam(':hostname', $hostname);
		$stmt->bindParam(':redeem_code', $redeem_code);
		$stmt->bindParam(':browser', $browser);
		$stmt->bindParam(':created', $created);
		$res = $stmt->execute();
	} catch (Exception $ex) {
		echo '{"status":"0"}';
		exit();
	}

	if ($res) {
		email($email, $preferred_date, $store, $redeem_link);
		echo '{"status":"1"}';
		exit();
	} else {
		echo '{"status":"0"}';
		exit();
	}
}