<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <?php include '_global.php'; ?>
    <?php
        $data = getApi('content/' . $_GET['nid'], 'post');
    ?>

    <title><?php print $data->data->title?> | #SocialHub</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?php print $data->data->body_plain?>" />

    <meta property="twitter:title" content="<?php print $data->data->title?> | #SocialHub" />
    <meta property="twitter:description" content="<?php print $data->data->body_plain?>" />
    <meta property="twitter:image" content="<?php print empty($data->data->image) ? $base_url . 'images/meta_img.jpg' : $data->data->image[0]->small->url?>" />

    <meta property="og:title" content="<?php print (!empty($meta_title) ? $meta_title : '') . (!empty($data->data->title) ? ' | ' . seoURL($data->data->title) : '') ?>" />
    <meta property="og:description" content="<?php print($data->data->body_plain); ?>" />
    <meta property="og:image" content="<?php print empty($data->data->image) ? $base_url . 'images/meta_img.jpg' : $data->data->image[0]->small->url?>" />
    <meta property="og:image:width" content="<?php print !empty($data->data->image->small->width) ? $data->data->image->small->width : 100 ?>" />
    <meta property="og:image:height" content="<?php print !empty($data->data->image->small->height) ? $data->data->image->small->height : 100 ?>"/>
    <meta property="og:type" content="website" />

    <!-- <script src="http://socialhub.innity-asia.com/microsite/default_min/?f=<?php //print $version?>/js/jquery.min.js"></script> -->

</head>
<body>

<form action="<?php print $base_url . (!empty($_GET['fb_share']) ? "?fb_share=" . $_GET['fb_share'] : "") ?>" method="post" id="postIDform">
    <input type="hidden" name="post_nid" value="<?php print ($_GET['nid'])?>">
    <input type="hidden" name="trafficsource" value="<?php print !empty($_GET['trafficsource']) ? $_GET['trafficsource'] : 'false' ?>">
</form>

<script type="text/javascript">
    document.getElementById("postIDform").submit();
</script>

</body>
</html>