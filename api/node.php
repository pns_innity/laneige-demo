<?php
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

if (!empty($_GET['nid'])) {
    include '_global.php';

    $data = getApi('content/' . $_GET['nid'], 'post');

}

if (!empty($data->data)) {
    $postCommentUrl = 'comment?content_id=' . $_GET['nid'] . '&brand_id=' . $brand_id . '&user_type=' . $ahc_user_type . '&limit=50&sort=created&order=asc';
    $postComments = getApi($postCommentUrl, 'comment');

    $allData = $postComments->data;
    while (!empty($postComments->paging->next)) {
        $apiUrl = 'comment?content_id=' . $_GET['nid'] . '&brand_id=' . $brand_id . '&user_type=' . $ahc_user_type . '&limit=50&sort=created&order=asc&after=' . $postComments->paging->cursors->after;
        $postComments = getApi($apiUrl, 'comment');
        $allData = array_merge($allData, $postComments->data);
    }

    $postComments->data = $allData;

    if ($postComments->data) {
        $commentUsers = [];
        $postCommentUsers = [];
        foreach ($postComments->data as $comment) {
            $commentUsers[] = $comment->user_id;
        }



        foreach ($commentUsers as $uid){
         $url = 'https://contenthub.innity-asia.com/2019/id/uphplus/eplus/public/api/0.2/user/custom/'.$uid.'.json';
         $ch =  curl_init($url);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
         $comment_data = curl_exec($ch);
         $postCommentUsers[] = json_decode($comment_data);
         curl_close($ch);



     }

 }
} else {
    return false;
}
$datas[] = $data->data;

if (!empty($postComments->data)) {
    $datas[] = $postComments->data;
    $datas[] = $postCommentUsers;
}


echo json_encode($datas);


//echo json_encode(array_merge(json_encode($data->data, true),json_encode($postComments, true)));

/*$datas[] = json_decode($data,true);
$datas[] = json_decode($postComments,true);

var_dump(json_encode($datas));*/
exit();
