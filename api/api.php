<?php
header('Access-Control-Allow-Origin: http://localhost:3000');
header('Access-Control-Allow-Credentials: true');
//header('Access-Control-Allow-Methods: POST');
ob_start();
include '_config.php';
require_once 'includes/helper.php';

$output = ob_get_clean();

//Pass API path
$endpoint = @$_GET['endpoint'];//user/reg @ api/comment.json
unset($_GET['endpoint']);

if (@$_GET['api']) {
    $api = $_GET['api'];
    unset($_GET['api']);
}

if (@$_GET['uid']) {
    $uid = $_GET['uid'];
    unset($_GET['uid']);
}

$access_token = getAccessToken();
$method = strtolower($_SERVER['REQUEST_METHOD']);
$data = [];
$header = [];

if ('get' == strtolower($_SERVER['REQUEST_METHOD'])) {
    $apiUrl = $base_api_url . $endpoint . '?user_id=' . $uid . '&user_type=' . $ahc_user_type . '&brand_id=' . $brand_id . '&access_token=' . $access_token;

    $ch =  curl_init($apiUrl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $data = curl_exec($ch);
    curl_close($ch);

    echo $data;
//echo json_encode($data); exit();
} else if ('post' == strtolower($_SERVER['REQUEST_METHOD'])) {
    $apiUrl = $base_api_url . $endpoint . '?brand_id=' . $brand_id;
    $uid = !empty($_POST['uid']) ? $_POST['uid'] : 0;
    $content_id = !empty($_POST['nid']) ? $_POST['nid'] : 0;

    $post = [
        "user_id" => $uid,
        "user_type" => $ahc_user_type,
        "content_id" => $content_id
    ];

    if ($endpoint == 'comment') {
        $post['comment'] = !empty($_POST['comment']) ? $_POST['comment'] : '';
        $post['status'] = true;
    }

    $authorization = "authorization: Bearer " . $access_token;
    $header = [
        $authorization,
        "content-type: application/json",
    ];

    $post = json_encode($post);

    $ch =  curl_init($apiUrl);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    curl_setopt($ch, CURLOPT_URL, $apiUrl);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    $data = curl_exec($ch);
    curl_close($ch);

    //echo json_encode($data);
}
exit();
