<?php

function getAccessToken()
{
    global $access_token_path, $access_token;

    if (file_exists($access_token_path . 'access_token.txt')) {
        $accessTokenData = file_get_contents($access_token_path . 'access_token.txt');
        $accessTokenData = json_decode($accessTokenData, true);
        if (!empty($accessTokenData)) {
            $expiredIn = $accessTokenData['expire_in'];
            if (time() < filemtime($access_token_path . 'access_token.txt') + $expiredIn) {
                $access_token = $accessTokenData['access_token'];
            } else {
                $access_token = generateAccessToken();
            }
        }
    } else {
        $access_token = generateAccessToken();

    }

    return $access_token;
}

function generateAccessToken()
{
    global $access_token_path, $access_token, $client_id, $client_secret, $base_api_domain_url, $ahc_username, $ahc_pwd;

    $tokenUrl = $base_api_domain_url . 'oauth/token';
    $params = [
        'client_id' => $client_id,
        'client_secret' => $client_secret,
        'grant_type' => 'password',
        'username' => $ahc_username,
        'password' => $ahc_pwd
    ];

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $tokenUrl);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $result = curl_exec($ch);

    if (!empty($result)) {
        file_put_contents($access_token_path . 'access_token.txt', $result);
    }
    $result = json_decode($result, true);
    $access_token = $result['access_token'];

    return $access_token;
}

function curlGet($url)
{
    global $base_api_url;

    $data = [];
    $query = parse_url($url, PHP_URL_QUERY);
    $url = $url . (($query === null) ? '?access_token=' : '&access_token=') . getAccessToken();

    $ch =  curl_init($base_api_url . $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $data = curl_exec($ch);
    $data = json_decode($data);
    curl_close($ch);

    return $data;
}

function seoURL($string)
{
    //Shorten the string
    $string = mb_substr($string, 0, 90,'UTF8');

    //Lower case everything
    $string = strtolower($string);
    //Remove all url format string
    $string =preg_replace("/\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|$!:,.;]*[A-Z0-9+&@#\/%=~_|$]/i", "", $string);
    //Make alphanumeric (removes all other characters)
    //$string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
    // \p{L} Matches any kind of letter from any language, \p{N} matches any kind of numeric character in any script, \n Matches a newline character.
    $string = preg_replace("~[^\p{L}\n\s_]+~u", "", $string);
    //Clean up multiple dashes or whitespaces
    $string = preg_replace("/[\s-]+/", " ", $string);
    //Trim first - avoid spaces on beginning or end of the text
    $string = trim($string);

    // Check whether is English
    $match = preg_match('/^[A-Za-z0-9 _]+$/', $string);

    $isEnglish = $match == 1;

    if ($isEnglish == 1) {
        $string = mb_substr($string, 0, 60);
        $strArray = explode(" ",$string);
        if (sizeof($strArray) != 1) {
            unset($strArray[intval(sizeof($strArray))-1]);
        }
        $string = implode(' ',$strArray);
    }else{
        $string = mb_substr($string, 0, 20,'UTF8');
    }

    //Convert whitespaces and underscore to dash
    $string = preg_replace("/[\s_]/", "-", $string);

    //Check empty string
    if (empty($string)) {
        $string = 'untitled';
    }
    return $string;
}

function wwwToNonWww()
{
    if (substr($_SERVER['SERVER_NAME'], 0, 3) == 'www') {
        $pageURL = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
        if ($_SERVER["SERVER_PORT"] != "80") {
            $pageURL .= substr($_SERVER['SERVER_NAME'], 4).":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
        } else {
            $pageURL .= substr($_SERVER['SERVER_NAME'], 4).$_SERVER["REQUEST_URI"];
        }

        header('location: '.$pageURL);
        exit();
    }
}

function checkCache()
{
    global $development_msg;

    $dir = array("cache/", "cache/access_token/", "cache/post/", "cache/comment/");

    for ($a=1; $a<sizeof($dir); $a++) {
        if (!is_writable($dir[$a])) {
            array_push($development_msg, 'Cache folder is not writable');
            break;
        }
    }
}

function clearLocalCache()
{
    global $cache_path, $cache_expiry;
    $caches = scandir($cache_path);

    if (!empty($caches)) {
        foreach ($caches as $file) {
            if (is_file($cache_path . $file) && file_exists($cache_path . $file) && ((time() - filemtime($cache_path . $file)) > $cache_expiry)) {
                unlink($cache_path . $file);
            }
        }
    }
}

function currentPageUrl()
{
    $pageURL = 'http';
    if (!empty($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {
        $pageURL .= "s";
    }
    $pageURL .= "://";
    if ($_SERVER["SERVER_PORT"] != "80") {
        $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
    } else {
        $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
    }
    return $pageURL;
}

function getCookie()
{
    global  $brand_id;

    $cookie = '';

    foreach ($_COOKIE as $key => $val) {
        if (substr($key, 0, 4) == 'SESS' && $key != 'SESSsocialhub_admin_'.$brand_id) {
            $cookie .= $key . '=' . urlencode($val) . '; ';
        }
    }

    return $cookie;
}


function getApi($url, $filetype) {
    global $cache_path, $cache_expiry, $base_api_url, $development;
    $data = [];

    $path = str_replace('/', '_', parse_url($url, PHP_URL_PATH));
    $query = parse_url($url, PHP_URL_QUERY);
    $filename = $path . (!empty($query) ? '&' . $query : '') . '.txt';
    $url = $url . (($query === null) ? '?access_token=' : '&access_token=') . getAccessToken();

    if ($filetype == "content") {
        $filename = md5($filename);
    }

    if ($development == false && privateIP($_SERVER['REMOTE_ADDR']) == false){
        // if file exists or not yet expire, return cache
        if (file_exists($cache_path . $filetype . '/' . $filename) && (time() - $cache_expiry < filemtime($cache_path . $filetype . '/' . $filename))) {
            $data = file_get_contents($cache_path . $filetype . '/' . $filename);
        }
        // if file didn't exist or expire
        else {
            $ch =  curl_init($base_api_url . $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $data = curl_exec($ch);
            curl_close($ch);

            $dataJson = json_decode($data);

            if(!empty($dataJson->data)){
                file_put_contents($cache_path . $filetype . '/' . $filename, $data);
            }
        }
    } else {
        $ch =  curl_init($base_api_url . $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $data = curl_exec($ch);
        curl_close($ch);
    }

    if(empty($data)){
        echo 'Network error, please try again.';exit();
    }

    clearLocalCache();
    $data = json_decode($data);

    return $data;
}

function clearCache($wildcard)
{
    global $cache_path;

    if (!empty($wildcard)) {
        $caches_folder = array("comment/");
    } else {
        $caches_folder = array("post/","comment/","access_token/","content/");
    }

    if (!empty($caches_folder)) {
        for ($i = 0; $i < count($caches_folder); $i++ ) {
            $cache_sub_path = $cache_path . $caches_folder[$i];
            $caches = scandir($cache_sub_path);

            for ($a = 2; $a < count($caches); $a++ ) {
                if (substr($caches[$a], 0, strlen($wildcard)) == $wildcard) {
                    unlink($cache_sub_path . $caches[$a]);
                }
            }
        }
    }
}

function extractHashtag($string)
{
    preg_match_all("/(#\w+)/", $string, $matches);
    foreach ($matches[0] as $key => $item) {
        $matches[0][$key] = str_replace('#', '', $item);
    }
    return $matches[0];
}

function systemConnect()
{
   global $cache_path, $cache_expiry, $base_api_domain_url,$user, $brand_id;

   $cookie = '';
   $url = $base_api_domain_url . 'api/system/connect.json';
   $cookie = getCookie();

    $opts = array(
        'http' => array(
            'method' => "POST",
            'header' => "Accept-language: en\r\n" .
            "Cookie: " . $cookie . "\r\n"
        )
    );

    // pass cookie header to file_get_contents
    $context = stream_context_create($opts);
    $data = file_get_contents($url, false, $context);

    if ($data) {
        $data = json_decode($data);
        $user = $data->user;
        $user->is_admin = false;

        if (!empty($user->admin)) {
            $user->admin = (array) json_decode($user->admin);

            if (in_array(strval($brand_id), $user->admin)) {
                $user->is_admin = true;
            }
        }
    }
}

function removeParam($url, $param)
{
    $url = preg_replace('/(&|\?)'.preg_quote($param).'=[^&]*$/', '', $url);
    $url = preg_replace('/(&|\?)'.preg_quote($param).'=[^&]*&/', '$1', $url);
    return $url;
}

function privateIP($ip)
{
    $pri_addrs = array (
                      '10.0.0.0|10.255.255.255', // single class A network
                      '172.16.0.0|172.31.255.255', // 16 contiguous class B network
                      '192.168.0.0|192.168.255.255', // 256 contiguous class C network
                      '169.254.0.0|169.254.255.255', // Link-local address also refered to as Automatic Private IP Addressing
                      '127.0.0.0|127.255.255.255' // localhost
                     );
    if ($ip == '::1') {
        return true;
    }
    $long_ip = ip2long ($ip);
    if ($long_ip != -1) {

        foreach ($pri_addrs AS $pri_addr) {
            list ($start, $end) = explode('|', $pri_addr);

            // IF IS PRIVATE
            if ($long_ip >= ip2long ($start) && $long_ip <= ip2long ($end)) {
                return true;
            }
        }
    }

    return false;
}

?>
